/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.hubspot.jinjava.Jinjava;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.extension.parameter.completers.FixedCompleter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;
import com.tinubu.logexplorer.jinjava.filters.AnsiFilter;
import com.tinubu.logexplorer.jinjava.filters.LogLevelFilter;
import com.tinubu.logexplorer.jinjava.filters.PadFilter;
import com.tinubu.logexplorer.jinjava.filters.ToDateFilter;

/**
 * Jinjava template support.
 * <p>
 * Additional filters supported :
 * <ul>
 * <li>ansi : {@link AnsiFilter}</li>
 * <li>toDate : {@link ToDateFilter}</li>
 * <li>logLevel : {@link LogLevelFilter}</li>
 * <li>pad : {@link PadFilter}</li>
 * </ul>
 *
 * @see <a href="http://jinja.pocoo.org/docs/2.10/templates/#">Jinja documentation</a>
 */
public class JinjavaFormatter implements Formatter {

   /** Unsupported characters in Jinjava identifiers. */
   private static final String STRIP_IDENTIFIER_CHARACTERS = "@";
   private static final String STRIP_IDENTIFIER_REPLACEMENT_CHARACTER = "_";

   public static final String PARAMETERS_CONTEXT_KEY = "parameters";
   public static final String TIMEZONE_CONTEXT_KEY = "timezone";

   private final String template;
   private final Jinjava jinjava;

   public JinjavaFormatter(Parameters parameters, String template, ZoneId timezone) {
      this.template = notNull(template, "template");

      jinjava = new Jinjava();
      jinjava.getGlobalContext().registerFilter(new AnsiFilter());
      jinjava.getGlobalContext().registerFilter(new ToDateFilter());
      jinjava.getGlobalContext().registerFilter(new LogLevelFilter());
      jinjava.getGlobalContext().registerFilter(new PadFilter());
      jinjava.getGlobalContext().put(PARAMETERS_CONTEXT_KEY, notNull(parameters, "parameters"));
      jinjava.getGlobalContext().put(TIMEZONE_CONTEXT_KEY, notNull(timezone, "timezone"));
   }

   @Override
   public void format(StringBuilder stringBuilder, Attributes attributes) {

      String line = jinjava.render(template, bindings(attributes));
      stringBuilder.append(line);
   }

   private Map<String, Object> bindings(Attributes attributes) {
      Map<String, Object> bindings = new HashMap<>();
      attributes.forEach((k, v) -> bindings.put(normalizeKey(k), v));
      return bindings;
   }

   private String normalizeKey(String key) {
      return StringUtils.replaceChars(key,
                                      STRIP_IDENTIFIER_CHARACTERS,
                                      STRIP_IDENTIFIER_REPLACEMENT_CHARACTER);
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Collections.singletonList(new ParameterDescription<>("jinjava.filter.todate.parse-timezone",
                                                                  ZoneId.class,
                                                                  "timezone used to complete parsing of local date times in <<plugin-jinjava-todate-filter,toDate filter>> to generate a ZonedDateTime.\n"
                                                                  + "Local system timezone will be used if this parameter is unset").completer(
            new FixedCompleter(new ArrayList<>(ZoneId.getAvailableZoneIds()))));
   }

   @Override
   public String displayString() {
      return JinjavaFormatterService.FORMATTER_NAME;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("template", template).append("jinjava", jinjava).toString();
   }
}
