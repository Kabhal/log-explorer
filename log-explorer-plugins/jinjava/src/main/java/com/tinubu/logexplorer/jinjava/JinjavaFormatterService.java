/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.jinjava;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.ZoneId;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.formatter.FormatterService;

public final class JinjavaFormatterService implements FormatterService {

   static final String FORMATTER_NAME = "jinjava";

   @Override
   public String name() {
      return FORMATTER_NAME;
   }

   public Formatter instance(Parameters parameters, ZoneId timezone, String template) {
      notNull(parameters, "parameters");
      notNull(template, "template");
      notNull(timezone, "timezone");

      return new JinjavaFormatter(parameters, template, timezone);
   }
}
