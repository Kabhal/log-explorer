/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.elasticsearch6x;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.io.IOException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.lang.TypeConverter;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;
import com.tinubu.logexplorer.core.search.query.NativeQuery;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

public abstract class AbstractElasticsearchBackend implements Backend {

   private static final Logger logger = LoggerFactory.getLogger(AbstractElasticsearchBackend.class);

   protected static final String DATE_TIME_NO_MILLIS_FORMAT = "date_time_no_millis";
   protected static final String DATE_TIME_FORMAT = "date_time";

   protected static final TimeValue SCROLL_TIMEOUT = TimeValue.timeValueSeconds(20);

   /** Page size parameter definition. */
   protected static final Pair<String, Integer> PAGE_SIZE_PARAMETER =
         Pair.of("elasticsearch.backend.page-size", 1000);
   /** Timestamp search field parameter definition. */
   protected static final Pair<String, String> TIMESTAMP_FIELD_PARAMETER =
         Pair.of("elasticsearch.backend.timestamp-field", "@timestamp");
   /** Disambiguation field parameter definition. */
   protected static final Pair<String, String> DISAMBIGUATION_FIELD_PARAMETER =
         Pair.of("elasticsearch.backend.disambiguation-field", null);
   /** Elasticsearch index parameter definition. */
   private static final Pair<String, String> CLIENT_INDEX_PARAMETER =
         Pair.of("elasticsearch.backend.index", "*");
   /** Follow mode interval parameter definition. */
   protected static final Pair<String, Integer> FOLLOW_WAIT_INTERVAL_PARAMETER =
         Pair.of("elasticsearch.backend.follow-interval", 1000);
   /** Verify SSL parameter definition. */
   protected static final Pair<String, Boolean> VERIFY_SSL_PARAMETER =
         Pair.of("elasticsearch.backend.verify-ssl", true);

   // TODO Feature flag highlighter
   private static final boolean HIGHLIGHT = false;
   /** Maximum duration in milliseconds between each request in follow mode. */
   private static final int MAX_FOLLOW_WAIT_INTERVAL = 3600000;

   protected final RestHighLevelClient client;
   private final RestClient lowLevelClient;
   protected final Parameters parameters;
   protected final BackendConfiguration configuration;
   protected final List<String> index;
   protected final int followWaitDuration;

   public AbstractElasticsearchBackend(Parameters parameters, BackendConfiguration configuration,
                                       boolean debug) {
      this.parameters = notNull(parameters, "parameters");
      this.configuration = notNull(configuration, "configuration");
      this.index = index(parameters);
      this.followWaitDuration = satisfies(parameters
                                                .intValue(FOLLOW_WAIT_INTERVAL_PARAMETER.getKey())
                                                .orElse(FOLLOW_WAIT_INTERVAL_PARAMETER.getValue()),
                                          followWaitDuration -> followWaitDuration >= 0
                                                                && followWaitDuration
                                                                   <= MAX_FOLLOW_WAIT_INTERVAL,
                                          "followWaitDuration",
                                          String.format("must be included in [0, %d]",
                                                        MAX_FOLLOW_WAIT_INTERVAL));

      Configurator.setLevel("org.elasticsearch.clientdebug",
                            Boolean.TRUE.equals(debug) ? Level.DEBUG : Level.INFO);

      final List<Header> headers = new ArrayList<>();
      if (configuration.authenticationToken() != null) {
         headers.add(bearerTokenHeader(configuration.authenticationToken()));
      }

      if (configuration.authenticationPassword() != null && configuration.authenticationUser() != null) {
         headers.add(basicHeader(configuration.authenticationUser(), configuration.authenticationPassword()));
      }

      logger.debug("Initialize Elasticsearch client with '{}'", configuration);

      RestClientBuilder restClientBuilder = RestClient
            .builder(configuration
                           .backendUris()
                           .stream()
                           .map(TypeConverter::urlValue)
                           .map(AbstractElasticsearchBackend::httpHost)
                           .toArray(HttpHost[]::new))
            .setHttpClientConfigCallback(clientConfig -> {
               HttpAsyncClientBuilder clientBuilder = clientConfig.setDefaultHeaders(headers);
               if (!verifySsl()) {
                  clientConfig.setSSLHostnameVerifier((s, sslSession) -> true);
               }
               return clientBuilder;
            })
            .setRequestConfigCallback(request -> request
                  .setConnectTimeout(configuration.connectTimeout())
                  .setSocketTimeout(configuration.socketTimeout()));

      client = new RestHighLevelClient(restClientBuilder);
      lowLevelClient = client.getLowLevelClient();
   }

   private boolean verifySsl() {
      return parameters.booleanValue(VERIFY_SSL_PARAMETER.getKey()).orElse(VERIFY_SSL_PARAMETER.getValue());
   }

   private String timestampField() {
      return parameters
            .stringValue(TIMESTAMP_FIELD_PARAMETER.getKey())
            .orElse(TIMESTAMP_FIELD_PARAMETER.getValue());
   }

   private String disambiguationField() {
      return parameters
            .stringValue(DISAMBIGUATION_FIELD_PARAMETER.getKey())
            .orElse(DISAMBIGUATION_FIELD_PARAMETER.getValue());
   }

   private int pageSize() {
      return parameters.intValue(PAGE_SIZE_PARAMETER.getKey()).orElse(PAGE_SIZE_PARAMETER.getValue());
   }

   private List<String> index(Parameters parameters) {
      return nullable(parameters.stringListValue(CLIENT_INDEX_PARAMETER.getKey())).orElse(singletonList(
            CLIENT_INDEX_PARAMETER.getValue()));
   }

   private BasicHeader bearerTokenHeader(String token) {
      notNull(token, "token");
      return new BasicHeader("Authorization", "Bearer " + token);
   }

   private BasicHeader basicHeader(String user, String password) {
      notNull(user, "user");
      notNull(password, "password");
      String raw = user + ":" + password;
      byte[] encode = Base64.getEncoder().encode(raw.getBytes());
      return new BasicHeader("Authorization", "Basic " + new String(encode));
   }

   private static HttpHost httpHost(URL u) {
      return new HttpHost(u.getHost(), u.getPort(), u.getProtocol());
   }

   public void close() {
      logger.debug("Close Elasticsearch client");

      try {
         lowLevelClient.close();
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   protected void callAction(BiConsumer<SearchResult, QuerySpecification> action,
                             QuerySpecification querySpecification,
                             SearchResponse searchResponse,
                             Long maxResults) {
      notNull(searchResponse, "searchResponse");
      Stream<SearchHit> resultStream = Arrays.stream(searchResponse.getHits().getHits());
      if (maxResults != null) {
         resultStream = resultStream.limit(maxResults);
      }
      SearchResult searchResult = new SearchResult(resultStream.map(this::attributes).collect(toList()),
                                                   searchResponse.getHits().getTotalHits());

      action.accept(searchResult, querySpecification);
   }

   protected Attributes attributes(SearchHit h) {
      Attributes attributes = new Attributes(h.getSourceAsMap());

      // h.getHighlightFields().forEach((field, highlight) -> attributes.put(field, highlight.fragments()[0]));

      return attributes;
   }

   protected CountRequest countRequest(QuerySpecification querySpecification, QueryDateRange dateRange) {
      return new CountRequest().source(searchSourceBuilder(querySpecification,
                                                           dateRange.fromDate(),
                                                           dateRange.toDate()));
   }

   protected SearchRequest searchNthRequest(QuerySpecification querySpecification,
                                            ZonedDateTime fromDate,
                                            ZonedDateTime toDate,
                                            Integer nth) {
      SearchSourceBuilder searchBuilder = searchSourceBuilder(querySpecification, fromDate, toDate);
      searchBuilder.sort(timestampField(), SortOrder.DESC);
      nullable(disambiguationField()).ifPresent(disambiguationField -> searchBuilder.sort(disambiguationField,
                                                                                          SortOrder.DESC));
      searchBuilder.from(nth);
      searchBuilder.size(1);
      return new SearchRequest(index.toArray(String[]::new)).source(searchBuilder);
   }

   protected SearchRequest searchScrollRequest(QuerySpecification querySpecification,
                                               ZonedDateTime fromDate,
                                               ZonedDateTime toDate) {
      SearchSourceBuilder searchBuilder = searchSourceBuilder(querySpecification, fromDate, toDate);
      searchBuilder.sort(timestampField(), SortOrder.ASC);
      searchBuilder.size(pageSize());
      return new SearchRequest(index.toArray(String[]::new)).source(searchBuilder).scroll(SCROLL_TIMEOUT);
   }

   protected SearchRequest searchAfterRequest(QuerySpecification querySpecification,
                                              ZonedDateTime fromDate,
                                              ZonedDateTime toDate,
                                              Object[] searchAfterValues) {
      SearchSourceBuilder searchBuilder = searchSourceBuilder(querySpecification, fromDate, toDate);

      if (searchAfterValues != null) {
         searchBuilder.searchAfter(searchAfterValues);
      }
      searchBuilder.sort(timestampField(), SortOrder.ASC);
      nullable(disambiguationField()).ifPresent(disambiguationField -> searchBuilder.sort(disambiguationField,
                                                                                          SortOrder.ASC));
      searchBuilder.size(pageSize());
      return new SearchRequest(index.toArray(String[]::new)).source(searchBuilder);
   }

   private SearchSourceBuilder searchSourceBuilder(QuerySpecification querySpecification,
                                                   ZonedDateTime fromDate,
                                                   ZonedDateTime toDate) {
      SearchSourceBuilder searchBuilder = new SearchSourceBuilder();

      RangeQueryBuilder rangeQuery = rangeQuery(timestampField()).format(DATE_TIME_NO_MILLIS_FORMAT);

      if (fromDate != null) {
         rangeQuery = rangeQuery.from(formattedDate(fromDate, DATE_TIME_NO_MILLIS_FORMAT));
      }
      if (toDate != null) {
         rangeQuery = rangeQuery.to(formattedDate(toDate, DATE_TIME_NO_MILLIS_FORMAT));
      }
      BoolQueryBuilder finalQuery = boolQuery().must(rangeQuery);

      for (NativeQuery queryElement : querySpecification.querySet(NativeQuery.class)) {
         finalQuery = finalQuery.filter(queryStringQuery(queryElement.queryString())
                                              .analyzeWildcard(true)
                                              .defaultField("*")
                                              .defaultOperator(Operator.AND));
      }
      for (NativeQuery notQueryElement : querySpecification.notQuerySet(NativeQuery.class)) {
         finalQuery = finalQuery.filter(queryStringQuery("!(" + notQueryElement.queryString() + ")")
                                              .analyzeWildcard(true)
                                              .defaultField("*")
                                              .defaultOperator(Operator.AND));
      }

      searchBuilder.query(finalQuery);

      if (HIGHLIGHT) {
         HighlightBuilder highlightBuilder = new HighlightBuilder();
         HighlightBuilder.Field highlightTitle = new HighlightBuilder.Field("app.message");
         highlightBuilder.field(highlightTitle);
         searchBuilder.highlighter(highlightBuilder);
      }

      return searchBuilder;
   }

   /**
    * Clear scroll id if not {@code null}.
    *
    * @param scrollIds scroll ids to clear or empty list
    *
    * @return response or {@code null} if scroll id was empty
    */
   protected ClearScrollResponse clearScrollContext(List<String> scrollIds) {
      notNull(scrollIds, "scrollIds");

      logger.debug("Clear scroll ids {}", scrollIds);

      ClearScrollResponse response = null;
      if (!scrollIds.isEmpty()) {
         ClearScrollRequest request = new ClearScrollRequest();
         request.setScrollIds(scrollIds);
         try {
            response = client.clearScroll(request, RequestOptions.DEFAULT);
            if (!response.isSucceeded()) {
               logger.debug("Clear scroll context request failed with status {}", response.status());
            }
         } catch (IOException e) {
            throw new IllegalStateException(String.format("Error with '%s'", configuration.backendUris()), e);
         }
      }
      return response;
   }

   protected Long countResult(QuerySpecification querySpecification, QueryDateRange dateRange) {
      logger.debug("Count results for query set '{}' spanning '{}'", querySpecification, dateRange);

      try {
         return client.count(countRequest(querySpecification, dateRange), RequestOptions.DEFAULT).getCount();
      } catch (IOException e) {
         throw new IllegalStateException(String.format("Error with '%s'", configuration.backendUris()), e);
      }
   }

   private String formattedDate(ZonedDateTime date, String dateFormat) {
      String formattedDate;
      switch (dateFormat) {
         case DATE_TIME_NO_MILLIS_FORMAT:
            formattedDate =
                  date.truncatedTo(SECONDS).withFixedOffsetZone().format(DateTimeFormatter.ISO_DATE_TIME);
            break;
         case DATE_TIME_FORMAT:
            formattedDate =
                  date.truncatedTo(MILLIS).withFixedOffsetZone().format(DateTimeFormatter.ISO_DATE_TIME);
            break;
         default:
            throw new IllegalArgumentException(String.format("Unknown date format '%s'", dateFormat));
      }

      return formattedDate;
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(PAGE_SIZE_PARAMETER.getKey(),
                                                      Integer.class,
                                                      PAGE_SIZE_PARAMETER.getValue(),
                                                      "request page size"),
                           new ParameterDescription<>(FOLLOW_WAIT_INTERVAL_PARAMETER.getKey(),
                                                      Integer.class,
                                                      FOLLOW_WAIT_INTERVAL_PARAMETER.getValue(),
                                                      "interval duration in milliseconds between requests in follow mode"),
                           new ParameterDescription<>(TIMESTAMP_FIELD_PARAMETER.getKey(),
                                                      String.class,
                                                      TIMESTAMP_FIELD_PARAMETER.getValue(),
                                                      "optional timestamp field name"),
                           new ParameterDescription<>(DISAMBIGUATION_FIELD_PARAMETER.getKey(),
                                                      String.class,
                                                      DISAMBIGUATION_FIELD_PARAMETER.getValue(),
                                                      "optional disambiguation field name for lines having the same timestamp"),
                           new ParameterDescription<>(CLIENT_INDEX_PARAMETER.getKey(),
                                                      String.class,
                                                      CLIENT_INDEX_PARAMETER.getValue(),
                                                      "Elasticsearch comma separated indices"));
   }

}
