/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.json;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

// TODO pre-parse parseKeys and mergeKeys into List<List<.. like UniformQuery
public class JsonParser implements Parser {

   private static final Pair<String, List<String>> PARSE_KEYS_PARAMETER =
         Pair.of("json.parser.parse-keys", emptyList());
   private static final Pair<String, List<String>> MERGE_KEYS_PARAMETER =
         Pair.of("json.parser.merge-keys", emptyList());
   private static final Pattern JSON_KEY_UNESCAPE_PATTERN = Pattern.compile("((?:\\\\\\\\)*)\\\\\\.");
   private static final Pattern JSON_KEY_SPLIT_PATTERN = Pattern.compile("(?<!\\\\)\\.");

   private final List<String> parseKeys;
   private final List<String> mergeKeys;

   public JsonParser(Parameters parameters) {
      notNull(parameters, "parameters");

      parseKeys = parameters.stringListValue(PARSE_KEYS_PARAMETER.getKey());
      mergeKeys = parameters.stringListValue(MERGE_KEYS_PARAMETER.getKey());
   }

   @Override
   public SearchResult parse(SearchResult result) {
      return new SearchResult(result).mapResults(this::parseJson).mapResults(this::mergeJson);
   }

   private Attributes parseJson(Attributes attributes) {
      for (String parseJsonKey : parseKeys) {
         parseJson(parseJsonKey, attributes);
      }
      return attributes;
   }

   private Attributes mergeJson(Attributes attributes) {
      for (String mergeJsonKey : mergeKeys) {
         mergeJson(mergeJsonKey, attributes);
      }
      return attributes;
   }

   private Attributes parseJson(String jsonKey, Attributes attributes) {
      if (jsonKey.contains(".")) {
         String[] subkeys = JSON_KEY_SPLIT_PATTERN.split(jsonKey, 2);
         String rootKey = unescapeKey(subkeys[0]);
         Object rootAttributes = attributes.get(rootKey);
         if (rootAttributes instanceof Attributes) {
            attributes.put(rootKey, parseJson(unescapeKey(subkeys[1]), (Attributes) rootAttributes));
         }
      } else {
         Object attribute = attributes.get(unescapeKey(jsonKey));
         if (attribute instanceof String) {
            try {
               Object parsedAttributes = parseNode(new ObjectMapper().readTree((String) attribute));
               attributes.put(jsonKey, parsedAttributes);
            } catch (JsonProcessingException ignored) {
            }
         }
      }

      return attributes;
   }

   private Attributes mergeJson(String jsonKey, Attributes attributes) {
      String[] subkeys = JSON_KEY_SPLIT_PATTERN.split(jsonKey, 2);
      if (subkeys.length > 1) {
         String rootKey = unescapeKey(subkeys[0]).split("!", 1)[0];

         Object rootAttributes = attributes.get(rootKey);
         if (rootAttributes instanceof Attributes) {
            attributes.put(rootKey, mergeJson(unescapeKey(subkeys[1]), (Attributes) rootAttributes));
         }
      } else {
         String[] jsonKeyWithExclusions = unescapeKey(jsonKey).split("!");
         String jsonKeyName = jsonKeyWithExclusions[0];
         List<String> excludedChildKeys = Stream.of(jsonKeyWithExclusions).skip(1).collect(toList());

         Object mergeAttributes = attributes.get(jsonKeyName);
         if (mergeAttributes instanceof Attributes) {
            attributes.remove(jsonKeyName);
            Attributes mergeAttributesWithoutExclusions = (Attributes) mergeAttributes;
            mergeAttributesWithoutExclusions.entrySet().removeIf(e -> excludedChildKeys.contains(e.getKey()));
            attributes.putAll(mergeAttributesWithoutExclusions);
         }

      }
      return attributes;
   }

   private static String unescapeKey(String key) {
      return JSON_KEY_UNESCAPE_PATTERN.matcher(key).replaceAll("$1.").replaceAll("\\\\\\\\", "\\\\");
   }

   /**
    * @return attribute value for the specified node or an {@link Attributes} sub-node
    */
   private Object parseNode(JsonNode node) {
      if (node.isObject()) {
         Attributes attributes = new Attributes();
         node.fields().forEachRemaining(entry -> attributes.put(entry.getKey(), parseNode(entry.getValue())));
         return attributes;
      } else if (node.isNull()) {
         return null;
      } else if (node.isArray()) {
         return node.toString();
      } else if (node.isValueNode()) {
         if (node.isBoolean()) {
            return node.booleanValue();
         } else if (node.isDouble()) {
            return node.doubleValue();
         } else if (node.isFloat()) {
            return node.floatValue();
         } else if (node.isInt()) {
            return node.intValue();
         } else if (node.isLong()) {
            return node.longValue();
         } else if (node.isShort()) {
            return node.shortValue();
         } else if (node.isTextual()) {
            return node.textValue();
         } else {
            return node.asText();
         }
      } else {
         return node.asText();
      }
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      return Arrays.asList(new ParameterDescription<>(PARSE_KEYS_PARAMETER.getKey(),
                                                      List.class,
                                                      PARSE_KEYS_PARAMETER.getValue(),
                                                      "attribute keys containing JSON to parse"),
                           new ParameterDescription<>(MERGE_KEYS_PARAMETER.getKey(),
                                                      List.class,
                                                      MERGE_KEYS_PARAMETER.getValue(),
                                                      "attribute keys containing JSON to merge (Use `<key>!<sub-key1>!<sub-key2>` to exclude some sub-keys from merge)"));
   }

}