/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.elasticsearch7x;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static com.tinubu.logexplorer.core.search.query.QueryBuilder.nativeQuery;

import java.io.IOException;
import java.util.Objects;
import java.util.function.BiConsumer;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.query.NativeQuery;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

public class SyncElasticsearchBackend extends AbstractElasticsearchBackend {

   private static final Logger logger = LoggerFactory.getLogger(SyncElasticsearchBackend.class);

   private BiConsumer<SearchResult, QuerySpecification> action;

   public SyncElasticsearchBackend(Parameters parameters, BackendConfiguration configuration, boolean debug) {
      super(parameters, configuration, debug);
   }

   public void search(BiConsumer<SearchResult, QuerySpecification> action,
                      QuerySpecification querySpecification,
                      QueryDateRange dateRange,
                      Long numberLines,
                      boolean followMode) {
      this.action = notNull(action, "action");
      notNull(querySpecification, "querySet");
      if (querySpecification.querySet(NativeQuery.class).isEmpty() && querySpecification
            .notQuerySet(NativeQuery.class)
            .isEmpty()) {
         querySpecification = querySpecification.addQuery(nativeQuery("*"));
      }
      if (numberLines != null) {
         satisfies(numberLines, nbl -> nbl >= 0, "numberLines", "must be >= 0");
      }

      logger.debug("Search Elasticsearch for query set '{}' spanning '{}' (numberLines={} follow={})",
                   querySpecification,
                   dateRange,
                   numberLines,
                   followMode);

      PageTracking pageTracking = new PageTracking(querySpecification, dateRange, numberLines, followMode);

      if (followMode || numberLines != null) {
         searchAfter(pageTracking);
      } else {
         searchScroll(pageTracking);
      }
   }

   private SearchResponse searchNth(int nth, PageTracking pageTracking) {
      try {
         return client.search(searchNthRequest(pageTracking.querySet(),
                                               pageTracking.fromDate(),
                                               pageTracking.toDate(),
                                               nth), RequestOptions.DEFAULT);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Starts a "search after".
    *
    * @param pageTracking page tracking
    */
   private void searchAfter(PageTracking pageTracking) {
      if (pageTracking.numberLines() != null) {
         pageTracking.initializeTracking(searchNth(pageTracking.numberLines().intValue(), pageTracking));
      } else {
         pageTracking.initializeTracking();
      }

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         try {
            while (searchAfterNextPage(client.search(searchAfterRequest(pageTracking.querySet(),
                                                                        pageTracking.fromDate(),
                                                                        pageTracking.toDate(),
                                                                        pageTracking.nextPageSortValues()),
                                                     RequestOptions.DEFAULT), pageTracking)) ;
         } catch (IOException e) {
            throw new IllegalStateException(String.format("Error using '%s'", configuration.backendUris()),
                                            e);
         }
      }
   }

   /**
    * Manages a paginated response from a "search after" and returns {@code true} while search should
    * continue.
    *
    * @param searchResponse paginated response received
    * @param pageTracking page tracking
    *
    * @return {@code true} if search should continue
    */
   private boolean searchAfterNextPage(SearchResponse searchResponse, PageTracking pageTracking) {
      pageTracking.trackResponse(searchResponse);

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         callAction(action,
                    pageTracking.querySet(),
                    searchResponse,
                    pageTracking.remainingLinesNumber().orElse(null));
         if (pageTracking.followMode()) {
            try {
               Thread.sleep(followWaitDuration);
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();
            }
         }
         return true;
      } else {
         callAction(action,
                    pageTracking.querySet(),
                    searchResponse,
                    pageTracking.remainingLinesNumber().orElse(null));
         return false;
      }
   }

   /**
    * Starts a "scroll search".
    *
    * @param pageTracking page tracking
    */
   private void searchScroll(PageTracking pageTracking) {
      satisfies(pageTracking.numberLines(),
                Objects::isNull,
                "pageTracking.numberLines",
                "numberLines must be null");
      pageTracking.initializeTracking();

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         try {
            SearchResponse searchResponse;
            if (scrollNextPage(searchResponse = client.search(searchScrollRequest(pageTracking.querySet(),
                                                                                  pageTracking.fromDate(),
                                                                                  pageTracking.toDate()),
                                                              RequestOptions.DEFAULT), pageTracking)) {

               while (scrollNextPage(searchResponse =
                                           client.scroll(new SearchScrollRequest(searchResponse.getScrollId()).scroll(
                                                 SCROLL_TIMEOUT), RequestOptions.DEFAULT), pageTracking)) ;
            }
         } catch (IOException e) {
            throw new IllegalStateException(String.format("Error using '%s'", configuration.backendUris()),
                                            e);
         }
      }
   }

   /**
    * Manages a paginated response from a "scroll search" and returns {@code true} while search should
    * continue.
    *
    * @param searchResponse paginated response received
    * @param pageTracking page tracking
    *
    * @return {@code true} if search should continue
    */
   private boolean scrollNextPage(SearchResponse searchResponse, PageTracking pageTracking) {
      pageTracking.trackResponse(searchResponse);

      if (!BackendInterruptionState.interrupting() && pageTracking.shouldRequestNextPage()) {
         callAction(action,
                    pageTracking.querySet(),
                    searchResponse,
                    pageTracking.remainingLinesNumber().orElse(null));
         if (pageTracking.followMode()) {
            try {
               Thread.sleep(followWaitDuration);
            } catch (InterruptedException e) {
               Thread.currentThread().interrupt();
            }
         }
         return true;
      } else {
         callAction(action,
                    pageTracking.querySet(),
                    searchResponse,
                    pageTracking.remainingLinesNumber().orElse(null));
         return false;
      }
   }

}
