/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.elasticsearch7x;

import com.tinubu.logexplorer.core.backend.Backend;
import com.tinubu.logexplorer.core.backend.BackendConfiguration;
import com.tinubu.logexplorer.core.backend.BackendService;
import com.tinubu.logexplorer.core.config.Configuration.Parameters;

public final class ElasticsearchBackendService implements BackendService {

   private static final String BACKEND_NAME = "elasticsearch-v7";
   private static final Boolean DEFAULT_SYNC_MODE = true;

   @Override
   public String name() {
      return BACKEND_NAME;
   }

   public Backend instance(Parameters parameters, BackendConfiguration backendConfiguration, boolean debug) {
      if (parameters.booleanValue("elasticsearch.backend.sync").orElse(DEFAULT_SYNC_MODE)) {
         return new SyncElasticsearchBackend(parameters, backendConfiguration, debug);
      } else {
         return new AsyncElasticsearchBackend(parameters, backendConfiguration, debug);
      }
   }
}
