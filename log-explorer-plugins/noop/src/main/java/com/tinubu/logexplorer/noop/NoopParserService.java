/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.noop;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;
import com.tinubu.logexplorer.core.parser.Parser;
import com.tinubu.logexplorer.core.parser.ParserService;

public final class NoopParserService implements ParserService {

   private static final String PARSER_NAME = "noop";

   @Override
   public String name() {
      return PARSER_NAME;
   }

   public Parser instance(Parameters parameters) {
      notNull(parameters, "parameters");

      return new NoopParser();
   }

}
