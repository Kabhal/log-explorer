/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import java.util.List;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.core.lang.TypeConverter;

public class LinesCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "lines";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("number of latest lines or unlimited",
                                        "[<lines-number|'unlimited'>]");
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      Long numberLines = numberLines(options.size() > 1 ? options.get(1) : null);

      if (numberLines < 0) {
         context.error("Number lines value must be >= 0");
      }

      context.numberLines(numberLines);
   }

   private Long numberLines(String value) {
      Long numberLines = null;
      if (!(value == null || "unlimited".startsWith(value.toLowerCase()))) {
         numberLines = TypeConverter.longValue(value);
      }
      return numberLines;
   }
}
