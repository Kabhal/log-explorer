/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import java.util.List;

import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.core.lang.TypeConverter;

public class FollowCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "follow";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("enable/disable log follow mode", "[<boolean>]");
   }

   @Override
   public int minimumParameters() {
      return 1;
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      if (!"now".equals(context.toDate())) {
         context.error("toDate must be 'now' in follow mode");
      }

      if (options.size() > 1) {
         context.followMode(TypeConverter.booleanValue(options.get(1)));
      } else {
         context.followMode(!context.followMode());
      }
   }

   @Override
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(), BooleanCompleter.INSTANCE, NullCompleter.INSTANCE);
   }

}
