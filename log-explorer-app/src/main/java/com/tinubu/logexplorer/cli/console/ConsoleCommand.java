/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.fusesource.jansi.Ansi.ansi;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.fusesource.jansi.Ansi;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

public abstract class ConsoleCommand {
   /** Command name to enter in console. */
   public abstract String commandName();

   /** Minimum number of required parameters for command. */
   public int minimumParameters() {return 0;}

   /** Command execution. */
   public abstract void execute(ConsoleContext context, List<String> options);

   /** Alias for {@link #execute(ConsoleContext, List)}. */
   public void execute(ConsoleContext context, String... options) {
      execute(context, Arrays.asList(options));
   }

   /** Command completer. */
   public Completer completer(ConsoleContext context) {
      return new ArgumentCompleter(new CommandCompleter(), NullCompleter.INSTANCE);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ConsoleCommand that = (ConsoleCommand) o;
      return commandName().equals(that.commandName());
   }

   @Override
   public int hashCode() {
      return Objects.hash(commandName());
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("commandName", commandName()).toString();
   }

   public abstract CommandHelpDescription helpDescription();

   public static class CommandHelpDescription {

      private final String description;
      private final List<String> options;
      private String verboseDescription;

      public CommandHelpDescription(String description, List<String> options) {
         this.description = notNull(description, "description");
         this.options = notNull(options, "options");
      }

      public CommandHelpDescription(String description) {
         this(description, Collections.emptyList());
      }

      public CommandHelpDescription(String description, String... options) {
         this(description, Arrays.asList(options));
      }

      /** Command description. */
      public String description() {
         return description;
      }

      /** list of options. */
      public List<String> options() {
         return options;
      }

      public CommandHelpDescription withVerboseDescription(String verboseDescription) {
         this.verboseDescription = verboseDescription;
         return this;
      }

      public Optional<String> verboseDescription() {
         return nullable(verboseDescription);
      }
   }

   public static class VerboseDescriptionBuilder {

      private static final String NEWLINE = "\n";

      private final StringBuilder description = new StringBuilder();

      public VerboseDescriptionBuilder append(String text) {
         description.append(text);
         return this;
      }

      public VerboseDescriptionBuilder appendnl(String text) {
         description.append(text).append(NEWLINE);
         return this;
      }

      public VerboseDescriptionBuilder appendnl() {
         description.append(NEWLINE);
         return this;
      }

      public VerboseDescriptionBuilder append(String text, Ansi ansiCode) {
         description.append(ansiCode).append(text).append(ansi().reset());
         return this;
      }

      public VerboseDescriptionBuilder appendnl(String text, Ansi ansiCode) {
         description.append(ansiCode).append(text).append(ansi().reset()).append(NEWLINE);
         return this;
      }

      public String toString() {
         return description.toString();
      }
   }

   /** Completer matching console command name. */
   public class CommandCompleter extends StringsCompleter {
      public CommandCompleter() {
         super(commandName());
      }
   }

   /** Completer matching true or false. */
   public static class BooleanCompleter extends StringsCompleter {
      public static BooleanCompleter INSTANCE = new BooleanCompleter();

      private BooleanCompleter() {
         super("true", "false");
      }
   }

   /** Completer matching known {@link ZoneId}. */
   public static class ZoneIdCompleter extends StringsCompleter {
      public static ZoneIdCompleter INSTANCE = new ZoneIdCompleter();

      private ZoneIdCompleter() {
         super(ZoneId.getAvailableZoneIds());
      }
   }

   /**
    * Completer matching current word. Uses to match any text to ensure working {@link
    * ArgumentCompleter#setStrict(boolean) strict mode}.
    */
   public static class AnyMatchCompleter implements Completer {
      @Override
      public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
         candidates.add(new Candidate(line.word()));
      }
   }

   /**
    * Strings completer with lazy supplier.
    */
   public static class LazyStringsCompleter implements Completer {
      private final Supplier<List<String>> candidateSupplier;

      public LazyStringsCompleter(Supplier<List<String>> candidateSupplier) {
         this.candidateSupplier = notNull(candidateSupplier, "candidateSupplier");
      }

      @Override
      public void complete(LineReader reader, ParsedLine line, List<Candidate> candidates) {
         new StringsCompleter(candidateSupplier.get()).complete(reader, line, candidates);
      }
   }

}
