/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.console.commands;

import static com.tinubu.commons.lang.util.CollectionUtils.list;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;

import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.converters.FlexibleDateTimeConverter;

public class FromDateCommand extends ConsoleCommand {

   @Override
   public String commandName() {
      return "fromDate";
   }

   @Override
   public CommandHelpDescription helpDescription() {
      return new CommandHelpDescription("query from date", "<date|duration|'now'>").withVerboseDescription(
            verboseDescription());
   }

   private String verboseDescription() {
      // @formatter:off
      return new VerboseDescriptionBuilder()
            .appendnl("Date syntax can be either : ")
            .appendnl("- a date : a precise date-time")
            .appendnl("- a duration : a duration in past from current time")
            .appendnl("- 'now' : current time")
            .appendnl()
            .appendnl("Date-time format is ISO-8601 (lenient), e.g. :")
            .appendnl("- 2024-02-01")
            .appendnl("- 2024-02-01T10:12:24")
            .appendnl("- 2024-02-01T10:12:24+02:00")
            .appendnl("- 2024-02-01T10:12:24+02")
            .appendnl("- 2024-02-01T10:12:24Z")
            .appendnl("- 2024-02-01T10:12:24[Europe/Paris]")
            .appendnl("If time is not specified, 00:00:00 is assumed")
            .appendnl("If offset/timezone is not specified, currently configured timezone is assumed")
            .appendnl()
            .appendnl("Duration format is ISO-8601, e.g. :")
            .appendnl("- PT1H")
            .appendnl("- PT30S")
            .appendnl("- P1D")
            .appendnl().toString();
      // @formatter:on
   }

   @Override
   public int minimumParameters() {
      return 1;
   }

   @Override
   public void execute(ConsoleContext context, List<String> options) {
      String fromDate = options.get(1);

      if (context.fromZonedDateTime(fromDate).isAfter(context.toZonedDateTime())) {
         context.error("fromDate must be <= toDate");
      }

      context.fromDate(fromDate);
   }

   /**
    * {@inheritDoc}
    * <p>
    * The following completing choices are proposed depending on current from date value :
    * <ul>
    *    <li>'now' : now as date-time</li>
    *    <li>date-time : date-time, duration from date-time to now</li>
    *    <li>duration : duration, date-time of (now - duration) point in time</li>
    * </ul>
    */
   @Override
   public Completer completer(ConsoleContext context) {

      return new ArgumentCompleter(new CommandCompleter(), new LazyStringsCompleter(() -> {
         ZoneId timezone = context.configuration().timezone();
         FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter(timezone);
         String fromDate = context.fromDate();

         return converter.parseNowAsDateTime(fromDate).map(now -> {
            return list(format(now));
         }).or(() -> converter.parseDuration(fromDate).map(duration -> {
            return list(fromDate, format(converter.now().minus(duration)));
         })).or(() -> converter.parseDateTime(fromDate).map(dateTime -> {
            return list(format(dateTime), format(Duration.between(dateTime, converter.now())));
         })).orElseGet(() -> list());
      }), NullCompleter.INSTANCE);
   }

   private String format(ZonedDateTime dateTime) {
      return dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
   }

   private String format(Duration duration) {
      return duration.toString();
   }

}
