/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.converters;

import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

import com.tinubu.commons.lang.util.Try;

import picocli.CommandLine.ITypeConverter;

public class FlexibleDateTimeConverter implements ITypeConverter<ZonedDateTime> {

   private final Clock clock;

   public FlexibleDateTimeConverter(Clock clock) {
      this.clock = clock;
   }

   public FlexibleDateTimeConverter(ZoneId timezone) {
      this.clock = timezone != null ? Clock.system(timezone) : Clock.systemDefaultZone();
   }

   public FlexibleDateTimeConverter() {
      this(Clock.systemDefaultZone());
   }

   @Override
   public ZonedDateTime convert(String value) {
      return parseNowAsDateTime(value)
            .or(() -> parseDurationAsDateTime(value))
            .or(() -> parseDateTime(value))
            .orElseThrow(() -> new IllegalArgumentException(String.format("Can't parse date '%s'", value)));
   }

   public Optional<ZonedDateTime> parseNowAsDateTime(String value) {
      if ("now".equals(value)) {
         return optional(now());
      } else {
         return optional();
      }
   }

   public Optional<ZonedDateTime> parseDateTime(String value) {
      try {
         DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive().parseLenient()
               .append(ISO_LOCAL_DATE)
               .optionalStart()
               .appendLiteral('T').append(ISO_LOCAL_TIME).optionalEnd()
               .optionalStart().appendOffsetId().optionalEnd()
               .optionalStart()
               .appendLiteral('[')
               .parseCaseSensitive()
               .appendZoneRegionId()
               .appendLiteral(']')
               .optionalEnd()
               .toFormatter();

         TemporalAccessor temporalAccessor =
               formatter.parseBest(value, ZonedDateTime::from, LocalDateTime::from, LocalDate::from);
         if (temporalAccessor instanceof ZonedDateTime) {
            return optional(((ZonedDateTime) temporalAccessor));
         }
         if (temporalAccessor instanceof LocalDateTime) {
            return optional(((LocalDateTime) temporalAccessor).atZone(clock.getZone()));
         }
         return optional(((LocalDate) temporalAccessor).atStartOfDay(clock.getZone()));
      } catch (DateTimeParseException e) {
         return optional();
      }
   }

   public Optional<ZonedDateTime> parseDurationAsDateTime(String value) {
      return parseDuration(value).map(d -> now().minus(d));
   }

   public Optional<Duration> parseDuration(String value) {
      try {
         return Try.handleException(() -> Duration.parse(value), DateTimeParseException.class).optional();
      } catch (Exception e) {
         throw sneakyThrow(e);
      }
   }

   public ZonedDateTime now() {
      return ZonedDateTime.now(clock).truncatedTo(ChronoUnit.SECONDS);
   }

}
