/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.logexplorer.LogExplorer.APPLICATION_NAME;
import static java.lang.System.exit;
import static java.util.stream.Collectors.joining;
import static org.fusesource.jansi.internal.CLibrary.isatty;
import static org.jline.reader.LineReader.Option.AUTO_FRESH_LINE;
import static org.jline.reader.LineReader.Option.DISABLE_EVENT_EXPANSION;
import static picocli.CommandLine.Command;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URI;
import java.nio.file.Paths;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.fusesource.jansi.AnsiConsole;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.history.DefaultHistory;
import org.jline.terminal.Terminal;
import org.jline.terminal.Terminal.SignalHandler;
import org.jline.terminal.TerminalBuilder;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.logexplorer.cli.console.ConsoleCommand;
import com.tinubu.logexplorer.cli.console.ConsoleContext;
import com.tinubu.logexplorer.cli.console.commands.AliasCommand;
import com.tinubu.logexplorer.cli.console.commands.BackendCommand;
import com.tinubu.logexplorer.cli.console.commands.ContextCommand;
import com.tinubu.logexplorer.cli.console.commands.DebugCommand;
import com.tinubu.logexplorer.cli.console.commands.DumpConfigCommand;
import com.tinubu.logexplorer.cli.console.commands.ExitCommand;
import com.tinubu.logexplorer.cli.console.commands.FollowCommand;
import com.tinubu.logexplorer.cli.console.commands.FormatterCommand;
import com.tinubu.logexplorer.cli.console.commands.FromDateCommand;
import com.tinubu.logexplorer.cli.console.commands.HelpCommand;
import com.tinubu.logexplorer.cli.console.commands.LinesCommand;
import com.tinubu.logexplorer.cli.console.commands.OutputCommand;
import com.tinubu.logexplorer.cli.console.commands.ParserCommand;
import com.tinubu.logexplorer.cli.console.commands.ProfileCommand;
import com.tinubu.logexplorer.cli.console.commands.QueryCommand;
import com.tinubu.logexplorer.cli.console.commands.ResetCommand;
import com.tinubu.logexplorer.cli.console.commands.SetCommand;
import com.tinubu.logexplorer.cli.console.commands.StatusCommand;
import com.tinubu.logexplorer.cli.console.commands.TimezoneCommand;
import com.tinubu.logexplorer.cli.console.commands.ToDateCommand;
import com.tinubu.logexplorer.cli.console.commands.UnaliasCommand;
import com.tinubu.logexplorer.cli.console.commands.UnprofileCommand;
import com.tinubu.logexplorer.core.backend.BackendInterruptionState;
import com.tinubu.logexplorer.core.config.AbstractConfiguration;
import com.tinubu.logexplorer.core.config.Configuration.AnsiFlag;
import com.tinubu.logexplorer.core.config.Configuration.Formatter;
import com.tinubu.logexplorer.core.config.Configuration.ProgressMeterFlag;
import com.tinubu.logexplorer.core.config.DirectConfiguration;
import com.tinubu.logexplorer.core.config.MapConfiguration;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.query.Query;
import com.tinubu.logexplorer.core.search.query.QueryBuilder;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = APPLICATION_NAME, description = "Universal log explorer", version = {
      APPLICATION_NAME + " ${git.build.version:--} (${git.build.time:--})",
      "JVM: ${java.version} (${java.vendor} ${java.vm.name} ${java.vm.version})",
      "Git: ${git.commit.id.abbrev:--} (${git.commit.time:--})" }, mixinStandardHelpOptions = true,
         sortOptions = false, parameterListHeading = "\n", optionListHeading = "\n")
@SuppressWarnings("squid:S106")
public class LogExplorer implements Runnable {

   private static final ExtendedLogger logger = ExtendedLogger.of(LogExplorer.class);

   static final String APPLICATION_NAME = "log-explorer";
   private static final File HISTORY_FILE =
         Paths.get(System.getProperty("user.home"), ".log-explorer.history").toFile();
   private static final Integer HISTORY_FILE_SIZE = 500000;
   private static final Integer HISTORY_SIZE = 500;
   private static final int USAGE_HELP_WIDTH = 120;

   static {
      ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
   }

   @Parameters(index = "*", description = "Query string.", defaultValue = "", paramLabel = "<query>")
   private List<String> queryParts;

   @Option(names = { "-f", "--from-date" }, arity = "1",
           description = "Query from date (inclusive) or duration from now [${DEFAULT-VALUE}].",
           defaultValue = "PT1H", paramLabel = "<date>")
   private String fromDate;

   @Option(names = { "-t", "--to-date" }, arity = "1", description = "Query to date (inclusive) [now].",
           paramLabel = "<date>", defaultValue = "now")
   private String toDate;

   @Option(names = { "-p", "--profiles" },
           description = "Configuration profiles by ascending priority order.", paramLabel = "<profile>")
   @SuppressWarnings("FieldMayBeFinal")
   private LinkedHashSet<String> profiles = new LinkedHashSet<>();

   @Option(names = { "-c", "--config" }, arity = "1", description = "Configuration file.",
           paramLabel = "<file>")
   private File configFile;

   @Option(names = { "-o", "--output" }, arity = "1", description = "Output file.", paramLabel = "<file>")
   private File outputFile;

   @Option(names = { "-a", "--append" }, description = "Append to output file.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean appendOutputFile = false;

   @Option(names = { "-D" }, description = "Configuration key/value.", paramLabel = "<key>=<value>")
   @SuppressWarnings("FieldMayBeFinal")
   private Map<String, String> cliConfig = new HashMap<>();

   @Option(names = { "-d", "--debug" }, description = "Debug mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean debugMode = false;

   @Option(names = { "--ansi" }, arity = "1", description = "ANSI colors.",
           paramLabel = "<auto|never|always>")
   private AnsiFlag ansiFlag;

   @Option(names = { "--progress" }, arity = "1", description = "Progress meter.",
           paramLabel = "<auto|never|always>")
   private ProgressMeterFlag progressMeterFlag;

   @Option(names = { "-i", "--interactive" }, description = "Interactive console mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean consoleMode = false;

   @Option(names = { "-n" }, arity = "1", description = "Number of latest lines to return.")
   private Long numberLines;

   @Option(names = { "-F", "--follow" }, description = "Follow mode.")
   @SuppressWarnings("FieldMayBeFinal")
   private boolean followMode = false;

   @Option(names = { "-z", "--timezone" }, description = "Timezone.")
   @SuppressWarnings("FieldMayBeFinal")
   private ZoneId timezone;

   @Option(names = { "--backend" }, arity = "1", description = "Backend.", paramLabel = "<backend>")
   private String backend;

   @Option(names = { "--backend-uri" }, arity = "1..*", description = "Backend URI(s).", paramLabel = "<URI>")
   private List<URI> backendUris;

   @Option(names = { "--parser" }, arity = "1", description = "Parser.", paramLabel = "<parser>")
   private String parser;

   @Option(names = { "--formatter" }, arity = "1", description = "Formatter.", paramLabel = "<formatter>")
   private String formatter;

   @Option(names = { "--file" }, arity = "1", description = "Open local file (shortcut mode).",
           paramLabel = "<file>|<URI>")
   private URI file;

   public static void main(String[] args) {
      loadGitProperties();

      exit(new LogExplorer().parse(args));
   }

   /** Entry point. */
   public int parse(String[] args) {
      CommandLine commandLine = new CommandLine(this);
      commandLine.setToggleBooleanFlags(false);
      commandLine.setCaseInsensitiveEnumValuesAllowed(true);
      commandLine.setUsageHelpWidth(USAGE_HELP_WIDTH);

      return commandLine.execute(args);
   }

   /**
    * Main loop.
    */
   @Override
   public void run() {
      try {
         if (followMode && !"now".equals(toDate)) {
            throw new IllegalArgumentException("toDate must be 'now' in follow mode");
         }
         if (consoleMode && !ttyTerminal()) {
            throw new IllegalArgumentException("Interactive mode can't be used in non-tty consoles");
         }

         MapConfiguration mapConfiguration = new MapConfiguration(cliConfig);
         AbstractConfiguration directConfiguration = new DirectConfiguration()
               .withBackend(backend)
               .withParser(parser)
               .withFormatter(formatter == null ? null : new Formatter(formatter))
               .withBackendUris(backendUris)
               .withAnsi(ansiFlag)
               .withProgress(progressMeterFlag)
               .withNumberLines(numberLines)
               .withTimezone(timezone);

         if (file != null) {
            directConfiguration.withBackend("file");
            directConfiguration.withBackendUri(file);
         }

         try (Context context = Context.create(fromDate,
                                               toDate,
                                               outputFile,
                                               appendOutputFile, profiles, debugMode,
                                               followMode,
                                               consoleMode,
                                               configFile, mapConfiguration, directConfiguration)) {

            configureAnsiConsole(context.configuration().ansi());

            mainLoop(context);
         }
      } catch (Exception e) {
         generalError(e);
      }
   }

   /**
    * Searches and loads git.properties file into system properties.
    */
   private static void loadGitProperties() {
      try (InputStream gitPropertiesStream = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream("git.properties")) {
         if (gitPropertiesStream != null) {
            Properties gitProperties = new Properties();
            gitProperties.load(gitPropertiesStream);

            gitProperties.forEach((k, v) -> System.setProperty(k.toString(), v.toString()));
         }
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   private void mainLoop(Context context) throws IOException {
      if (consoleMode) {
         interactiveConsole(context);
      } else {
         QuerySpecification querySpecification = QuerySpecification
               .empty()
               .withQuerySet(context.configuration().query().flatten())
               .withNotQuerySet(context.configuration().notQuery().flatten());

         querySpecification = cliQuery().map(querySpecification::addQuery).orElse(querySpecification);

         context
               .search()
               .search(querySpecification,
                       new QueryDateRange(context.fromZonedDateTime(), context.toZonedDateTime()),
                       context.configuration().numberLines(),
                       context.followMode());
      }
   }

   /** Generates CLI query from argument parts. */
   private Optional<Query> cliQuery() {
      String queryString = String.join(" ", queryParts).trim();

      if (!queryString.isEmpty()) {
         return Optional.of(QueryBuilder.parseQuery(queryString));
      } else {
         return Optional.empty();
      }
   }

   /**
    * Manages uncaught exception in non interactive mode.
    *
    * @param e uncaught exception
    */
   private void generalError(Exception e) {
      System.err.println("[E] " + e.getMessage());
      logger.debug(e::getMessage, e);
      exit(1);
   }

   /**
    * Configures {@link AnsiConsole} depending on resolved {@link AnsiFlag} mode.
    * <p>
    * This global {@link org.fusesource.jansi.Ansi} configuration is dedicated to console exporters.
    *
    * @implSpec {@link org.fusesource.jansi.Ansi#setEnabled(boolean)} is disabled to strip ANSI codes
    *       from {@link org.fusesource.jansi.Ansi::ansi()} commands when console is not ANSI-enabled.
    * @implSpec {@code jansi.force=true} property is used to always enable the output console to
    *       support ANSI codes even when console is not a tty.
    * @implSpec in auto mode, if stdout is not a tty, ANSI codes are disabled even if progress meter
    *       is enabled (it outputs to stderr but priority is given to performance).
    */
   private void configureAnsiConsole(AnsiFlag ansi) {
      switch (ansi) {
         case AUTO:
            if (ttyTerminal()) {
               logger.debug("ANSI auto mode : Setup ANSI console for tty console");
               if (ideaTerminal()) {
                  System.setProperty("jansi.force", "true");
               }
               AnsiConsole.systemInstall();
               Runtime.getRuntime().addShutdownHook(new Thread(AnsiConsole::systemUninstall));
            } else {
               logger.debug("ANSI auto mode : Disable ANSI code generation for non tty console");
               org.fusesource.jansi.Ansi.setEnabled(false);
            }
            break;
         case NEVER:
            logger.debug("ANSI never mode : Disable ANSI code generation");
            org.fusesource.jansi.Ansi.setEnabled(false);
            break;
         case ALWAYS:
            logger.debug("ANSI always mode : Setup ANSI console (force)");
            System.setProperty("jansi.force", "true");
            AnsiConsole.systemInstall();
            Runtime.getRuntime().addShutdownHook(new Thread(AnsiConsole::systemUninstall));
            break;
      }
   }

   /**
    * Returns true if running in a TTY terminal.
    *
    * @return true if running in a TTY terminal.
    */
   private boolean ttyTerminal() {
      return isatty(1) != 0 || ideaTerminal();
   }

   /**
    * Detects if IntelliJ IDEA terminal is used.
    *
    * @return true if running application in IDEA terminal.
    */
   private boolean ideaTerminal() {
      try {
         Class<?> phClass = Class.forName("java.lang.ProcessHandle");
         Object current = phClass.getMethod("current").invoke(null);
         Object parent = ((Optional<?>) phClass.getMethod("parent").invoke(current)).orElse(null);
         Method infoMethod = phClass.getMethod("info");
         Object info = infoMethod.invoke(parent);
         Object command =
               ((Optional<?>) infoMethod.getReturnType().getMethod("command").invoke(info)).orElse(null);
         return command != null && ((String) command).contains("idea");
      } catch (Exception t) {
         return false;
      }
   }

   /**
    * @implNote {@link LineReader.Option#AUTO_FRESH_LINE} is required to cleanup progress meter when
    *       it is enabled.
    */
   private void interactiveConsole(Context context) throws IOException {
      Terminal terminal = TerminalBuilder.builder().name(APPLICATION_NAME)
            .jna(false)
            .jansi(true)
            .signalHandler(interactiveConsoleSignalHandler())
            .build();
      ConsoleCommand helpCommand = new HelpCommand();
      List<ConsoleCommand> commands = Arrays.asList(helpCommand,
                                                    new StatusCommand(),
                                                    new SetCommand(),
                                                    new TimezoneCommand(),
                                                    new FromDateCommand(),
                                                    new ToDateCommand(),
                                                    new QueryCommand(),
                                                    new ProfileCommand(),
                                                    new UnprofileCommand(),
                                                    new ResetCommand(),
                                                    new LinesCommand(),
                                                    new FollowCommand(),
                                                    new OutputCommand(),
                                                    new ContextCommand(),
                                                    new DebugCommand(),
                                                    new DumpConfigCommand(),
                                                    new AliasCommand(),
                                                    new UnaliasCommand(),
                                                    new BackendCommand(),
                                                    new ParserCommand(),
                                                    new FormatterCommand(),
                                                    new ExitCommand());
      try (ConsoleContext consoleContext = new ConsoleContext(context, terminal, commands)) {
         LineReader reader = lineReader(terminal, consoleContext);
         Optional<Query> initialQuery = cliQuery();

         try {
            while (!consoleContext.exitConsole()) {
               String line;
               try {
                  line = reader.readLine(consoleContext.prompt(),
                                         null,
                                         initialQuery.map(Query::queryString).orElse(null));

                  if (!line.startsWith("#")) {
                     if (line.isBlank()) {
                        helpCommand.execute(consoleContext);
                     } else {
                        List<String> lineElements = Arrays.asList(line.split("\\s+"));

                        Optional<ConsoleCommand> currentCommand = matchCommand(consoleContext, lineElements);

                        currentCommand.ifPresentOrElse(command -> command.execute(consoleContext,
                                                                                  lineElements), () -> {
                           consoleContext.printError("Bad command syntax");
                           helpCommand.execute(consoleContext);
                        });
                     }
                  }
                  if (consoleContext.commandsCompleterUpdated()) {
                     reader = lineReader(terminal, consoleContext);
                     consoleContext.commandsCompleterUpdated(false);
                  }
               } catch (UserInterruptException e) {
                  /* Ignored. */
               } catch (EndOfFileException e) {
                  consoleContext.exitConsole(true);
               } catch (Exception e) {
                  consoleContext.printError(e);
               } finally {
                  BackendInterruptionState.interrupting(false);
                  initialQuery = Optional.empty();
               }
            }
         } finally {
            reader.getHistory().save();
         }
      }
   }

   private LineReader lineReader(Terminal terminal, ConsoleContext consoleContext) {
      return LineReaderBuilder
            .builder()
            .appName(APPLICATION_NAME)
            .completer(consoleContext.commandsCompleter())
            .history(new DefaultHistory())
            .option(DISABLE_EVENT_EXPANSION, true)
            .variable(LineReader.HISTORY_FILE, HISTORY_FILE.toString())
            .variable(LineReader.HISTORY_SIZE, HISTORY_SIZE)
            .variable(LineReader.HISTORY_FILE_SIZE, HISTORY_FILE_SIZE)
            .option(AUTO_FRESH_LINE, true)
            .terminal(terminal)
            .build();
   }

   /** Intercept ^C to interrupt backend execution. */
   private SignalHandler interactiveConsoleSignalHandler() {
      return signal -> BackendInterruptionState.interrupting(true);
   }

   /**
    * Search the command matching this line elements.
    * Command matching is case-insensitive.
    *
    * @param context console context
    * @param lineElements parsed line elements
    *
    * @return exact matching command, or first non-ambiguous matching command, or {@link Optional#empty} if no
    *       command found, or minimum number of parameters not provided for matched command.
    */
   private Optional<ConsoleCommand> matchCommand(ConsoleContext context, List<String> lineElements) {
      String commandLineCommand = lineElements.get(0);
      Optional<ConsoleCommand> exactCommand = optional();
      List<ConsoleCommand> ambiguousCommands = list();

      for (ConsoleCommand command : context.commands()) {
         if (StringUtils.equalsIgnoreCase(command.commandName(), commandLineCommand)) {
            exactCommand = optional(command);
         } else if (StringUtils.startsWithIgnoreCase(command.commandName(), commandLineCommand)) {
            ambiguousCommands.add(command);
         }
      }

      return exactCommand.or(() -> {
         if (ambiguousCommands.size() > 1) {
            context.error("Ambiguous commands : %s",
                          ambiguousCommands.stream().map(ConsoleCommand::commandName).collect(joining(",")));
         }

         return ambiguousCommands.stream().findFirst();
      }).filter(command -> lineElements.size() > command.minimumParameters());
   }

}
