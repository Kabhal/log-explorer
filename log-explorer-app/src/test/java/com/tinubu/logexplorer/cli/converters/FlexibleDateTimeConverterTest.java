/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.cli.converters;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

class FlexibleDateTimeConverterTest {

   private static final Clock FIXED_CLOCK = Clock.fixed(Instant.EPOCH, ZoneId.of("Europe/Paris"));

   @Test
   public void testConverterWhenSpecialDate() {
      FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter(FIXED_CLOCK);

      assertThat(converter.convert("now")).isEqualTo(ZonedDateTime.now(FIXED_CLOCK));
   }

   private void checkDateTime(FlexibleDateTimeConverter converter,
                              String dateTime,
                              int year,
                              int month,
                              int dayOfMonth,
                              int hour,
                              int minute,
                              int second,
                              int nanoOfSecond,
                              ZoneId zone) {
      assertThat(converter.convert(dateTime)).isEqualTo(ZonedDateTime.of(year,
                                                                         month,
                                                                         dayOfMonth,
                                                                         hour,
                                                                         minute,
                                                                         second,
                                                                         nanoOfSecond,
                                                                         zone));
   }

   @Test
   public void testConverterWhenIso8601DateTimeWithNoTimeZone() {
      FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter(FIXED_CLOCK);

      checkDateTime(converter, "2019-03-29", 2019, 3, 29, 0, 0, 0, 0, FIXED_CLOCK.getZone());
      checkDateTime(converter, "2019-03-29T15:02:06", 2019, 3, 29, 15, 2, 6, 0, FIXED_CLOCK.getZone());
      checkDateTime(converter,
                    "2019-03-29T15:02:06.666",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    666000000,
                    FIXED_CLOCK.getZone());
   }

   @Test
   public void testConverterWhenIso8601ZonedDateTime() {
      FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter(FIXED_CLOCK);

      checkDateTime(converter, "2019-03-29T15:02:06Z", 2019, 3, 29, 15, 2, 6, 0, ZoneId.of("UTC"));
      checkDateTime(converter, "2019-03-29T15:02:06.666Z", 2019, 3, 29, 15, 2, 6, 666000000, ZoneId.of("UTC"));
      checkDateTime(converter,
                    "2019-03-29T15:02:06+01:00",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    0,
                    ZoneId.ofOffset("", ZoneOffset.ofHours(1)));
      checkDateTime(converter,
                    "2019-03-29T15:02:06.666+01:00",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    666000000,
                    ZoneId.ofOffset("", ZoneOffset.ofHours(1)));
      checkDateTime(converter,
                    "2019-03-29T15:02:06+01:00[Europe/Berlin]",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    0,
                    ZoneId.of("Europe/Berlin"));
      checkDateTime(converter,
                    "2019-03-29t15:02:06+01:00[Europe/Berlin]",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    0,
                    ZoneId.of("Europe/Berlin"));
      checkDateTime(converter,
                    "2019-03-29T15:02:06[Europe/Berlin]",
                    2019,
                    3,
                    29,
                    15,
                    2,
                    6,
                    0,
                    ZoneId.of("Europe/Berlin"));
      checkDateTime(converter,
                    "2019-03-29[Europe/Berlin]",
                    2019,
                    3,
                    29,
                    0,
                    0,
                    0,
                    0,
                    ZoneId.of("Europe/Berlin"));
   }

   @Test
   public void testConverterWhenIso8601Duration() {
      FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter(FIXED_CLOCK);

      assertThat(converter.convert("P1D")).isEqualTo(ZonedDateTime.of(1969,
                                                                      12,
                                                                      31,
                                                                      1,
                                                                      0,
                                                                      0,
                                                                      0,
                                                                      FIXED_CLOCK.getZone()));
      assertThat(converter.convert("P-1DT4H")).isEqualTo(ZonedDateTime.of(1970,
                                                                          1,
                                                                          1,
                                                                          21,
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          FIXED_CLOCK.getZone()));
   }

   @Test
   public void testConverterWhenParseError() {
      FlexibleDateTimeConverter converter = new FlexibleDateTimeConverter();

      assertThatThrownBy(() -> converter.convert("unknown-special")).isInstanceOf(IllegalArgumentException.class);
      assertThatThrownBy(() -> converter.convert("2020-20-20 13:12:24")).isInstanceOf(IllegalArgumentException.class);
   }
}