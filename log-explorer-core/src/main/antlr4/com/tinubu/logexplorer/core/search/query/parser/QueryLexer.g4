lexer grammar QueryLexer ;

SIMPLE_QUERY: ('s' | 'simple')? '/' -> pushMode(simple_query) ;
NATIVE_QUERY: ('n' | 'native') '/'  -> pushMode(native_query) ;
UNIFORM_QUERY: ('u' | 'uniform') '/'  -> pushMode(uniform_query) ;

mode simple_query ;

S_TEXT: (~('/' | '\\') | '\\' ('/' | '\\'))+ ;
S_CLOSE_QUERY: '/' [a-z]* -> popMode ;

mode native_query ;

N_TEXT: (~('/' | '\\') | '\\' ('/' | '\\'))+ ;
N_CLOSE_QUERY: '/' [a-z]*  -> popMode ;

mode uniform_query ;

U_WS: [ \r\n\t]+ -> skip ;

U_NOT: '!' ;
U_AND: '&&' ;
U_OR: '||' ;
U_LPAR: '(' ;
U_RPAR: ')' ;

U_OPERATOR: '=' | '!=' | '~' | '!~' | '<' | '<=' | '>' | '>=' ;

U_NULL: 'null' ;
U_INTEGER: '-'? [0-9]+ ;
U_DECIMAL: '-'? [0-9]+ ('.' [0-9]+)? ;
U_BOOLEAN: ('true' | 'false') ;
U_STRING: '"' (~('"' | '\\') | '\\' ('"' | '\\'))* '"' ;
U_PATTERN: [a-z]* '/' (~('/' | '\\') | '\\' ('/' | '\\'))* '/' [a-z]* ;

U_IDENTIFIER: [a-zA-Z][a-zA-Z0-9_-]* ('\\'? '.' U_IDENTIFIER)? ;

U_CLOSE_QUERY: '/' [a-z]*  -> popMode ;