/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.backend;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;

/**
 * Extension point interface for backend plugins.
 */
public interface BackendService {

   /**
    * Uniquely identifies this backend.
    *
    * @return formatter name
    */
   String name();

   /**
    * Builds a new backend instance with the specified parameters and configuration.
    *
    * @param parameters plugins parameters map
    * @param backendConfiguration general backend client configuration
    * @param debug debug flag
    *
    * @return new backend instance
    */
   Backend instance(Parameters parameters, BackendConfiguration backendConfiguration, boolean debug);

}
