/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace.visitors;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.fusesource.jansi.Ansi.ansi;

import com.tinubu.logexplorer.core.stacktrace.StackTraceException;
import com.tinubu.logexplorer.core.stacktrace.TracedMethod;

/**
 * Filtering and ANSI enabled {@link OutputStringVisitor}.
 */
public class AnsiStackTraceVisitor extends OutputStringVisitor {

   public AnsiStackTraceVisitor() {
      super();
   }

   public AnsiStackTraceVisitor(TracedMethodFilter tracedMethodFilter) {
      super(tracedMethodFilter);
   }

   @Override
   protected String filterExceptionAssociation(StackTraceException exception, String exceptionAssociation) {
      return ansi().fgMagenta().a(exceptionAssociation).reset().toString();
   }

   @Override
   protected String filterQualifiedExceptionType(StackTraceException exception,
                                                 String exceptionType,
                                                 String qualifier) {
      return isEmpty(qualifier)
             ? ansi().fgBrightRed().bold().a(exceptionType).reset().toString()
             : ansi()
                   .fgBrightMagenta()
                   .a(qualifier)
                   .reset()
                   .a(" ")
                   .fgBrightRed()
                   .bold()
                   .a(exceptionType)
                   .reset()
                   .toString();
   }

   @Override
   protected String filterExceptionMessage(StackTraceException exception, String exceptionMessage) {
      return ansi().fgRed().a(exceptionMessage).reset().toString();
   }

   @Override
   protected String filterTracedMethodClassName(StackTraceException exception,
                                                TracedMethod tracedMethod,
                                                String className) {
      if (tracedMethodFilter.filteredIn(tracedMethod)) {
         return ansi().fgYellow().a(className).reset().toString();
      } else {
         return ansi().fgBlue().a(className).reset().toString();
      }
   }

   @Override
   protected String filterTracedMethodName(StackTraceException exception,
                                           TracedMethod tracedMethod,
                                           String methodName) {
      if (tracedMethodFilter.filteredIn(tracedMethod)) {
         return ansi().fgBrightYellow().a(methodName).reset().toString();
      } else {
         return ansi().fgBrightBlue().a(methodName).reset().toString();
      }
   }

   @Override
   protected String filterTracedMethodFileName(StackTraceException exception,
                                               TracedMethod tracedMethod,
                                               String fileName) {
      return ansi().fgBrightCyan().a(fileName).reset().toString();
   }

   @Override
   protected String filterTracedMethodLine(StackTraceException exception,
                                           TracedMethod tracedMethod,
                                           int methodLine) {
      return ansi().fgCyan().a(methodLine).reset().toString();
   }

   @Override
   protected boolean filterTracedMethod(StackTraceException exception, TracedMethod tracedMethod) {
      return tracedMethodFilter.filter(exception, tracedMethod);
   }

}
