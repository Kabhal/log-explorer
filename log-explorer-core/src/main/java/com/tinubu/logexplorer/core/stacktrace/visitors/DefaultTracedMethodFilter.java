/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace.visitors;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.List;

import com.tinubu.logexplorer.core.stacktrace.StackTraceException;
import com.tinubu.logexplorer.core.stacktrace.TracedMethod;

/**
 * Default trace method filter with basic configuration.
 */
public class DefaultTracedMethodFilter implements TracedMethodFilter {

   /** Whether to always include filtered-in trace methods, even in max traced methods counter is reached. */
   private static final boolean ALWAYS_INCLUDE_FILTERED_IN_METHODS = true;

   private final DefaultTracedMethodFilterConfiguration configuration;
   private int traceMethodsCounter = 0;

   public DefaultTracedMethodFilter() {
      this(new DefaultTracedMethodFilterConfiguration());
   }

   public DefaultTracedMethodFilter(DefaultTracedMethodFilterConfiguration configuration) {
      this.configuration = notNull(configuration, "configuration");
   }

   @Override
   public void resetCounter() {
      traceMethodsCounter = 0;
   }

   /**
    * @implSpec first {@link DefaultTracedMethodFilterConfiguration#minTracedMethods()} methods are
    *       always displayed.
    * @implSpec included methods have higher priority on excluded methods.
    */
   @Override
   public boolean filter(StackTraceException exception, TracedMethod tracedMethod) {
      traceMethodsCounter++;

      boolean filteredInPackages = filteredIn(tracedMethod);

      if (ALWAYS_INCLUDE_FILTERED_IN_METHODS && filteredInPackages) {
         return true;
      }

      return traceMethodsCounter <= configuration.minTracedMethods() || (traceMethodsCounter
                                                                         <= configuration.maxTracedMethods()
                                                                         && (filteredInPackages
                                                                             || !filteredOut(tracedMethod)));
   }

   @Override
   public boolean filteredIn(TracedMethod tracedMethod) {
      return matchAnyPackagePrefix(tracedMethod, configuration.includeTracedMethods());
   }

   @Override
   public boolean filteredOut(TracedMethod tracedMethod) {
      return matchAnyPackagePrefix(tracedMethod, configuration.excludeTracedMethods());
   }

   private boolean matchAnyPackagePrefix(TracedMethod tracedMethod, List<String> methodPrefixes) {
      return methodPrefixes.stream().anyMatch(methodPrefix -> {
         String method = tracedMethod.className().name() + "." + tracedMethod.methodName();
         return method.startsWith(methodPrefix);
      });
   }

   public static class DefaultTracedMethodFilterConfiguration {

      private static final int DEFAULT_MIN_TRACED_METHODS = 3;
      private static final int DEFAULT_MAX_TRACED_METHODS = 20;

      private int minTracedMethods = DEFAULT_MIN_TRACED_METHODS;
      private int maxTracedMethods = DEFAULT_MAX_TRACED_METHODS;
      private List<String> includeTracedMethods = new ArrayList<>();
      private List<String> excludeTracedMethods = new ArrayList<>();

      public DefaultTracedMethodFilterConfiguration() {
      }

      public DefaultTracedMethodFilterConfiguration(int minTracedMethods,
                                                    int maxTracedMethods,
                                                    List<String> includeTracedMethods,
                                                    List<String> excludeTracedMethods) {
         minMaxTracedMethods(minTracedMethods, maxTracedMethods);
         this.includeTracedMethods = notNull(includeTracedMethods, "includeTracedPackages");
         this.excludeTracedMethods = notNull(excludeTracedMethods, "excludeTracedPackages");
      }

      public DefaultTracedMethodFilterConfiguration minMaxTracedMethods(int minTracedMethods,
                                                                        int maxTracedMethods) {
         satisfies(minTracedMethods, min -> min >= 0, "minTracedMethods", "must be >= 0");
         satisfies(maxTracedMethods, max -> max >= 0, "maxTracedMethods", "must be >= 0");
         this.minTracedMethods = minTracedMethods;
         this.maxTracedMethods = max(minTracedMethods, maxTracedMethods);
         return this;
      }

      public int minTracedMethods() {
         return minTracedMethods;
      }

      public int maxTracedMethods() {
         return maxTracedMethods;
      }

      public List<String> includeTracedMethods() {
         return includeTracedMethods;
      }

      public DefaultTracedMethodFilterConfiguration includeTracedMethods(List<String> includeTracedPackages) {
         this.includeTracedMethods = includeTracedPackages;
         return this;
      }

      public List<String> excludeTracedMethods() {
         return excludeTracedMethods;
      }

      public DefaultTracedMethodFilterConfiguration excludeTracedMethods(List<String> excludeTracedPackages) {
         this.excludeTracedMethods = excludeTracedPackages;
         return this;
      }

   }
}
