/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.logexplorer.core.lang.TypeConverter.booleanValue;
import static com.tinubu.logexplorer.core.lang.TypeConverter.doubleValue;
import static com.tinubu.logexplorer.core.lang.TypeConverter.longValue;
import static com.tinubu.logexplorer.core.lang.TypeConverter.patternValue;
import static com.tinubu.logexplorer.core.lang.TypeConverter.stringValue;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.ParseCancellationException;

import com.tinubu.logexplorer.core.search.query.UniformQuery.NullOperand;
import com.tinubu.logexplorer.core.search.query.UniformQuery.Operator;
import com.tinubu.logexplorer.core.search.query.parser.QueryLexer;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.ComposedUniformQueryContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.NativeQueryContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.QueryAndContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.QueryGroupContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.QueryNegateContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.QueryNodeContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.QueryOrContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.SimpleQueryContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParser.UniformQueryContext;
import com.tinubu.logexplorer.core.search.query.parser.QueryParserBaseVisitor;
import com.tinubu.logexplorer.core.search.query.parser.QueryParserVisitor;

public class QueryBuilder {

   /**
    * Pattern to split uniform query attribute path in multiple segments, excluding escaped dots like
    * {@code field\.value}.
    */
   private static final Pattern UNIFORM_ATTRIBUTE_SPLIT_PATTERN = Pattern.compile("(?<!\\\\)\\.");
   /** Pattern to remove escaping backslashes from an attribute path segment. */
   private static final Pattern UNIFORM_ATTRIBUTE_UNESCAPE_PATTERN =
         Pattern.compile("((?:\\\\\\\\)*)\\\\\\.");

   public static SimpleQuery simpleQuery(String query) {
      return SimpleQuery.of(query);
   }

   public static SimpleQuery simpleQuery(String query, Set<SimpleQuery.Option> options) {
      return SimpleQuery.of(query, options);
   }

   public static NativeQuery nativeQuery(String query) {
      return NativeQuery.of(query);
   }

   public static NativeQuery nativeQuery(String query, String options) {
      return NativeQuery.of(query, options);
   }

   public static UniformQuery uniformQuery(List<String> fieldPath, Operator operator, Object operand) {
      return UniformQuery.of(fieldPath, operator, operand, emptySet());
   }

   public static UniformQuery uniformQuery(List<String> fieldPath,
                                           Operator operator,
                                           Object operand,
                                           Set<UniformQuery.Option> options) {
      return UniformQuery.of(fieldPath, operator, operand, options);
   }

   public static Query parseQuery(String query) {
      CharStream in = CharStreams.fromString(query);
      QueryLexer lexer = new QueryLexer(in);
      lexer.removeErrorListener(ConsoleErrorListener.INSTANCE);

      CommonTokenStream tokens = new CommonTokenStream(lexer);
      QueryParser parser = new QueryParser(tokens);

      parser.removeErrorListeners();
      parser.addErrorListener(ThrowingErrorListener.INSTANCE);

      QueryParserVisitor<Query> visitor = new QueryParserBaseVisitor<>() {

         private Set<UniformQuery.Option> uniformQueryOptions = new HashSet<>();

         @Override
         public Query visitSimpleQuery(SimpleQueryContext ctx) {
            String queryOptions = queryOptions(ctx.closeQuery);
            Set<SimpleQuery.Option> options = new HashSet<>();
            if (queryOptions.contains("i")) {
               options.add(SimpleQuery.Option.CASE_INSENSITIVE);
            }

            String simpleQuery = nullable(ctx.queryText)
                  .map(Token::getText)
                  .map(QueryBuilder::unquoteUndelimitedPattern)
                  .orElseThrow(() -> new IllegalArgumentException("Simple query is empty"));

            return simpleQuery(simpleQuery, options);
         }

         @Override
         public Query visitNativeQuery(NativeQueryContext ctx) {
            String queryOptions = queryOptions(ctx.closeQuery);

            String nativeQuery = nullable(ctx.queryText)
                  .map(Token::getText)
                  .map(QueryBuilder::unquoteUndelimitedPattern)
                  .orElseThrow(() -> new IllegalArgumentException("Native query is empty"));

            return nativeQuery(nativeQuery, queryOptions);
         }

         @Override
         public Query visitUniformQuery(UniformQueryContext ctx) {
            String queryOptions = queryOptions(ctx.closeQuery);
            if (queryOptions.contains("i")) {
               uniformQueryOptions.add(UniformQuery.Option.CASE_INSENSITIVE);
            }

            return ctx.getChild(ComposedUniformQueryContext.class, 0).accept(this);
         }

         @Override
         public Query visitQueryGroup(QueryGroupContext ctx) {
            return ctx.composedUniformQuery().accept(this);
         }

         @Override
         public Query visitQueryOr(QueryOrContext ctx) {
            throw new UnsupportedOperationException("Not yet implemented '||'");
         }

         @Override
         public Query visitQueryAnd(QueryAndContext ctx) {
            throw new UnsupportedOperationException("Not yet implemented '&&'");
         }

         @Override
         public Query visitQueryNegate(QueryNegateContext ctx) {
            throw new UnsupportedOperationException("Not yet implemented '!'");
         }

         @Override
         public Query visitQueryNode(QueryNodeContext ctx) {
            Object operand;
            switch (ctx.uniformQueryOperand().queryOperand.getType()) {
               case QueryLexer.U_STRING: {
                  operand = stringValue(unquoteString(ctx.uniformQueryOperand().queryOperand.getText()));
                  break;
               }
               case QueryLexer.U_PATTERN: {
                  operand = patternValue(ctx.uniformQueryOperand().queryOperand.getText());
                  break;
               }
               case QueryLexer.U_NULL: {
                  operand = new NullOperand();
                  break;
               }
               case QueryLexer.U_DECIMAL: {
                  operand = doubleValue(ctx.uniformQueryOperand().queryOperand.getText());
                  break;
               }
               case QueryLexer.U_INTEGER: {
                  operand = longValue(ctx.uniformQueryOperand().queryOperand.getText());
                  break;
               }
               case QueryLexer.U_BOOLEAN: {
                  operand = booleanValue(ctx.uniformQueryOperand().queryOperand.getText());
                  break;
               }
               default: {
                  throw new IllegalArgumentException(String.format("Unknown '%s' operand",
                                                                   lexer
                                                                         .getVocabulary()
                                                                         .getSymbolicName(ctx.uniformQueryOperand().queryOperand.getType())));
               }
            }

            List<String> fieldPath = emptyList();
            if (ctx.queryIdentifier != null) {
               fieldPath = Stream
                     .of(UNIFORM_ATTRIBUTE_SPLIT_PATTERN.split(ctx.queryIdentifier.getText()))
                     .map(QueryBuilder::unescapePathSegment)
                     .collect(toList());
            }
            return uniformQuery(fieldPath,
                                Operator.fromOperatorString(ctx.queryOperator.getText()),
                                operand,
                                uniformQueryOptions);
         }

      };

      try {
         return visitor.visitQuery(parser.query());
      } catch (ParseCancellationException e) {
         throw new IllegalArgumentException(String.format("Can't parse '%s' query", query), e);
      }
   }

   private static String unescapePathSegment(String attributePathSegment) {
      return UNIFORM_ATTRIBUTE_UNESCAPE_PATTERN
            .matcher(attributePathSegment)
            .replaceAll("$1.")
            .replaceAll("\\\\\\\\", "\\\\");
   }

   /**
    * Returns query options from closing query token.
    *
    * @param closeQuery close query token
    *
    * @return option list as string or empty string if none
    */
   private static String queryOptions(Token closeQuery) {
      return nullable(closeQuery).map(cq -> cq.getText().substring(1)).orElse("");
   }

   public static class ThrowingErrorListener extends BaseErrorListener {

      public static final ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

      @Override
      public void syntaxError(Recognizer<?, ?> recognizer,
                              Object offendingSymbol,
                              int line,
                              int charPositionInLine,
                              String msg,
                              RecognitionException e) throws ParseCancellationException {
         throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
      }
   }

   /**
    * Unquote query string :
    * <ul>
    * <li>"string" -> string</li>
    * <li>"stri\"ng" -> stri"ng</li>
    * <li>"stri\\ng" -> stri\ng</li>
    * <li>"stri\ng" -> stri\ng</li>
    * </ul>
    * No format validation is done in this function.
    *
    * @param operand query string operand
    *
    * @return unquoted string
    */
   private static String unquoteString(String operand) {
      return operand.substring(1, operand.length() - 1).replaceAll("\\\\([\\\\\"])", "$1");
   }

   /**
    * Unquote pattern internal part (no slashes delimiters) :
    * <ul>
    * <li>string -> string</li>
    * <li>stri\/ng -> stri/ng</li>
    * <li>stri\\ng -> stri\ng</li>
    * <li>stri\ng -> stri\ng</li>
    * </ul>
    * No format validation is done in this function.
    *
    * @param operand query pattern operand
    *
    * @return unquoted patter
    */
   private static String unquoteUndelimitedPattern(String operand) {
      return operand.replaceAll("\\\\([\\\\/])", "$1");
   }

}
