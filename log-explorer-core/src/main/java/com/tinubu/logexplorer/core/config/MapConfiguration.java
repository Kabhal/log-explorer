/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.config;

import static java.util.Optional.ofNullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.tinubu.logexplorer.core.lang.TypeConverter;

public class MapConfiguration extends AbstractConfiguration {
   private final Map<String, String> configuration;

   public MapConfiguration(Map<String, String> configuration) {
      this.configuration = new HashMap<>(configuration);

      value("backendUris").map(TypeConverter::uriListValue).ifPresent(this::withBackendUris);
      value("connectTimeout").map(TypeConverter::intValue).ifPresent(this::withConnectTimeout);
      value("socketTimeout").map(TypeConverter::intValue).ifPresent(this::withSocketTimeout);
      value("authenticationToken").ifPresent(this::withAuthenticationToken);
      value("authenticationUser").ifPresent(this::withAuthenticationUser);
      value("authenticationPassword").ifPresent(this::withAuthenticationPassword);
      value("ansi").map(AnsiFlag::valueOf).ifPresent(this::withAnsi);
      value("parser").ifPresent(this::withParser);
      value("formatter.name")
            .map(name -> new Formatter(name, value("formatter.template").orElse(null)))
            .ifPresent(this::withFormatter);
      value("progress").map(ProgressMeterFlag::valueOf).ifPresent(this::withProgress);
      value("timezone").map(TypeConverter::zoneIdValue).ifPresent(this::withTimezone);
      value("backend").ifPresent(this::withBackend);
      value("numberLines").map(TypeConverter::longValue).ifPresent(this::withNumberLines);

      withParameters(new Parameters(configuration));
   }

   private Optional<String> value(String value) {
      return ofNullable(configuration.remove(value));
   }
}
