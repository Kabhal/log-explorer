/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.config;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import com.tinubu.logexplorer.core.lang.TypeConverter;
import com.tinubu.logexplorer.core.search.query.QueryBuilder;

/**
 * YAML backed configuration.
 * <p>
 * Loads a {@link Configuration} from a YAML, given a set of profiles.
 */
public class YamlConfiguration extends AbstractConfiguration {

   private static final Logger logger = LoggerFactory.getLogger(YamlConfiguration.class);

   private static final String DEFAULT_PROFILE = "default";

   private static final Map<URI, ConfigurationModel> configurationCache = new HashMap<>();

   /**
    * Creates YAML configuration.
    *
    * @param resource YAML URI to read configuration from
    * @param profiles a set of profiles to load, by order of priority (less to higher priority)
    * @param ignoreIfNotFound whether to ignore if YAML resource is not available
    */
   public YamlConfiguration(URI resource, LinkedHashSet<String> profiles, boolean ignoreIfNotFound) {
      notNull(resource, "resource");

      ConfigurationModel configurationModel = loadConfigurationModel(resource, ignoreIfNotFound, true);

      if (configurationModel != null) {
         loadProfile(configurationModel, DEFAULT_PROFILE, true);
         for (String profile : profiles) {
            loadProfile(configurationModel, profile, false);
         }
      }
   }

   public static ConfigurationModel configurationModel(URI resource,
                                                       boolean ignoreIfNotFound,
                                                       boolean recurseIncludes) {
      notNull(resource, "resource");

      if (recurseIncludes) {
         return loadConfigurationModel(resource, ignoreIfNotFound, true);
      } else {
         return loadRawConfigurationModel(resource, ignoreIfNotFound);
      }
   }

   public static List<String> availableProfiles(URI resource,
                                                boolean ignoreIfNotFound,
                                                boolean recurseIncludes) {
      notNull(resource, "resource");

      ConfigurationModel configurationModel =
            loadConfigurationModel(resource, ignoreIfNotFound, recurseIncludes);

      if (configurationModel != null) {
         return new ArrayList<>(configurationModel.profiles.keySet());
      } else {
         return emptyList();
      }
   }

   private static ConfigurationModel loadConfigurationModel(URI resource,
                                                            boolean ignoreIfNotFound,
                                                            boolean recurseIncludes) {
      ConfigurationModel configurationModel = configurationCache.computeIfAbsent(resource, resourceUri -> {
         return loadRawConfigurationModel(resourceUri, ignoreIfNotFound);
      });

      if (recurseIncludes && configurationModel != null) {
         configurationModel.setProfiles(includedProfiles(configurationModel, resource));
      }

      return configurationModel;
   }

   private static ConfigurationModel loadRawConfigurationModel(URI resource, boolean ignoreIfNotFound) {
      logger.debug("Load configuration from '{}' (ignoreIfNotFound={})", resource, ignoreIfNotFound);

      try {
         InputStream resourceStream = resourceStream(resource, ignoreIfNotFound);

         if (resourceStream != null) {
            Yaml yaml = new Yaml();

            ConfigurationModel model = yaml.loadAs(resourceStream, ConfigurationModel.class);

            logger.debug("Loaded '{}' configuration model = {}", resource, model);
            return model;
         } else {
            logger.debug("Ignored '{}' not found configuration model", resource);
            return null;
         }
      } catch (Exception e) {
         throw new IllegalStateException(String.format("Can't load configuration '%s' > %s",
                                                       resource,
                                                       e.getMessage()), e);
      }
   }

   private static InputStream resourceStream(URI resourceUri, boolean ignoreIfNotFound) {
      InputStream resourceStream;
      if (resourceUri.getScheme().equals("classpath")) {
         resourceStream = loadClassPathResource(resourceUri, ignoreIfNotFound);
      } else {
         resourceStream = loadFileSystemResource(resourceUri, ignoreIfNotFound);
      }
      return resourceStream;
   }

   private void loadProfile(ConfigurationModel model, String profile, boolean ignoreIfNotFound) {
      notNull(profile, "profile");
      notNull(model, "model");

      if (logger.isDebugEnabled()) {
         logger.debug("Load profile {}", profile);
      }

      boolean profileNegate = profile.startsWith("!");
      String profileName = profile.substring(profileNegate ? 1 : 0);

      ConfigurationProfile profileConfig = model.getProfiles().get(profileName);

      if (profileConfig != null) {
         ofNullable(profileConfig.getExtends())
               .map(TypeConverter::stringListValue)
               .ifPresent(extendProfiles -> extendProfiles.forEach(extendProfile -> loadProfile(model,
                                                                                                extendProfile,
                                                                                                false)));

         if (!profileNegate) {
            ofNullable(profileConfig.getBackendUris()).ifPresent(this::withBackendUris);
            ofNullable(profileConfig.getConnectTimeout()).ifPresent(this::withConnectTimeout);
            ofNullable(profileConfig.getSocketTimeout()).ifPresent(this::withSocketTimeout);
            ofNullable(profileConfig.getAuthenticationToken()).ifPresent(this::withAuthenticationToken);
            ofNullable(profileConfig.getAuthenticationUser()).ifPresent(this::withAuthenticationUser);
            ofNullable(profileConfig.getAuthenticationPassword()).ifPresent(this::withAuthenticationPassword);
            ofNullable(profileConfig.getAnsi()).ifPresent(this::withAnsi);
            ofNullable(profileConfig.getParser()).ifPresent(this::withParser);
            ofNullable(profileConfig.getFormatter())
                  .map(t -> new Configuration.Formatter(t.getName(), t.getTemplate()))
                  .ifPresent(this::withFormatter);
            ofNullable(profileConfig.getProgress()).ifPresent(this::withProgress);
            ofNullable(profileConfig.getTimezone())
                  .map(TypeConverter::zoneIdValue)
                  .ifPresent(this::withTimezone);
            ofNullable(profileConfig.getBackend()).ifPresent(this::withBackend);
            ofNullable(profileConfig.getParameters()).ifPresent(this::withParameters);
            ofNullable(profileConfig.getQuery()).ifPresent(q -> withQuery(queryGroupFromConfig(q)));
            ofNullable(profileConfig.getNotQuery()).ifPresent(q -> withNotQuery(queryGroupFromConfig(q)));
            ofNullable(profileConfig.getNumberLines()).ifPresent(this::withNumberLines);
            ofNullable(profileConfig.getAliases()).ifPresent(this::withAliases);
         } else {
            ofNullable(profileConfig.getQuery()).ifPresent(q -> withNotQuery(queryGroupFromConfig(q)));
            ofNullable(profileConfig.getNotQuery()).ifPresent(q -> withQuery(queryGroupFromConfig(q)));
            ofNullable(profileConfig.getParameters())
                  .map(this::negateParameters)
                  .ifPresent(this::withParameters);
         }
      } else {
         if (!ignoreIfNotFound) {
            throw new IllegalArgumentException(String.format("Unknown profile '%s'", profile));
         }
      }
   }

   private QueryGroup queryGroupFromConfig(Map<String, List<String>> queryGroup) {
      return new QueryGroup(queryGroup
                                  .entrySet()
                                  .stream()
                                  .collect(toMap(Entry::getKey,
                                                 e -> e.getValue().stream().map(QueryBuilder::parseQuery)
                                                       .collect(toList()))));
   }

   /**
    * Recursively builds a map of configuration profiles from included resources.
    *
    * @param currentModel current model
    * @param currentResource current resource
    *
    * @return final map of all included profiles
    */
   private static Map<String, ConfigurationProfile> includedProfiles(ConfigurationModel currentModel,
                                                                     URI currentResource) {
      Map<String, ConfigurationProfile> profiles = new LinkedHashMap<>();
      for (URI include : TypeConverter.uriListValue(currentModel.getInclude())) {
         URI includeResource = includeResource(currentResource, include);
         ConfigurationModel includeModel = loadConfigurationModel(includeResource, false, true);
         if (includeModel != null) {
            profiles.putAll(includedProfiles(includeModel, includeResource));
         }
      }
      profiles.putAll(currentModel.getProfiles());
      return profiles;
   }

   /**
    * Generates a valid included resource {@link URI} from including resource URI.
    *
    * @param currentResource current resource URI to build relative resource from
    * @param include resource URI to include
    *
    * @return include resource {@link URI}
    */
   public static URI includeResource(URI currentResource, URI include) {
      return currentResource.resolve(currentResource.toString().endsWith("/") ? ".." : ".").resolve(include);
   }

   private Boolean negateBoolean(Boolean bool) {
      if (bool == null) {return null;} else {return !bool;}
   }

   /**
    * Returns only parameters of type boolean with their values negated.
    */
   private Parameters negateParameters(Parameters parameters) {
      Parameters negatedParameters = new Parameters();

      parameters.forEach((key, value) -> {
         if (value instanceof Boolean) {
            negatedParameters.put(key, !(Boolean) value);
         }
      });

      return negatedParameters;
   }

   private static InputStream loadFileSystemResource(URI resource, boolean ignoreIfNotFound) {
      notNull(resource, "resource");

      try {
         return new FileInputStream(new File(resource));
      } catch (FileNotFoundException e) {
         if (ignoreIfNotFound) {
            return null;
         } else {
            throw new IllegalArgumentException(String.format("Configuration not found '%s'", resource));
         }
      }
   }

   private static InputStream loadClassPathResource(URI resource, boolean ignoreIfNotFound) {
      notNull(resource, "resource");

      InputStream resourceAsStream = Thread
            .currentThread()
            .getContextClassLoader()
            .getResourceAsStream(StringUtils.stripStart(resource.getPath(), "/"));
      if (resourceAsStream == null) {
         if (ignoreIfNotFound) {
            return null;
         } else {
            throw new IllegalArgumentException(String.format("Configuration not found '%s'", resource));
         }
      }
      return resourceAsStream;
   }

   public static class ConfigurationModel {
      private Object include = new ArrayList<>();
      private Map<String, ConfigurationProfile> profiles = new LinkedHashMap<>();

      public Object getInclude() {
         return include;
      }

      public void setInclude(Object include) {
         this.include = include;
      }

      public Map<String, ConfigurationProfile> getProfiles() {
         return profiles;
      }

      public void setProfiles(Map<String, ConfigurationProfile> profiles) {
         this.profiles = profiles;
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("include", include).append("profiles", profiles).toString();
      }
   }

   public static class ConfigurationProfile {
      private List<URI> backendUris;
      private Integer connectTimeout;
      private Integer socketTimeout;
      private String authenticationToken;
      private String authenticationUser;
      private String authenticationPassword;
      private AnsiFlag ansi;
      private String parser;
      private Formatter formatter;
      private ProgressMeterFlag progress;
      private Map<String, List<String>> query;
      private Map<String, List<String>> notQuery;
      private Object timezone;
      private String backend;
      private Parameters parameters;
      private Object extendProfiles;
      private Long numberLines;
      private Aliases aliases;

      public List<URI> getBackendUris() {
         return backendUris;
      }

      public void setBackendUris(List<URI> backendUris) {
         this.backendUris = backendUris;
      }

      public Integer getConnectTimeout() {
         return connectTimeout;
      }

      public void setConnectTimeout(Integer connectTimeout) {
         this.connectTimeout = connectTimeout;
      }

      public Integer getSocketTimeout() {
         return socketTimeout;
      }

      public void setSocketTimeout(Integer socketTimeout) {
         this.socketTimeout = socketTimeout;
      }

      public String getAuthenticationToken() {
         return authenticationToken;
      }

      public void setAuthenticationToken(String authenticationToken) {
         this.authenticationToken = authenticationToken;
      }

      public String getAuthenticationUser() {return authenticationUser;}

      public void setAuthenticationUser(String authenticationUser) {
         this.authenticationUser = authenticationUser;
      }

      public String getAuthenticationPassword() {return authenticationPassword;}

      public void setAuthenticationPassword(String authenticationPassword) {
         this.authenticationPassword = authenticationPassword;
      }

      public AnsiFlag getAnsi() {
         return ansi;
      }

      public void setAnsi(AnsiFlag ansi) {
         this.ansi = ansi;
      }

      public String getParser() {
         return parser;
      }

      public void setParser(String parser) {
         this.parser = parser;
      }

      public Formatter getFormatter() {
         return formatter;
      }

      public void setFormatter(Formatter formatter) {
         this.formatter = formatter;
      }

      public ProgressMeterFlag getProgress() {
         return progress;
      }

      public void setProgress(ProgressMeterFlag progress) {
         this.progress = progress;
      }

      public Map<String, List<String>> getQuery() {
         return query;
      }

      public void setQuery(Map<String, List<String>> query) {
         this.query = query;
      }

      public Map<String, List<String>> getNotQuery() {
         return notQuery;
      }

      public void setNotQuery(Map<String, List<String>> notQuery) {
         this.notQuery = notQuery;
      }

      public Object getTimezone() {return timezone;}

      public void setTimezone(Object timezone) {this.timezone = timezone;}

      public String getBackend() {return this.backend;}

      public void setBackend(String backend) {
         this.backend = backend;
      }

      public Parameters getParameters() {
         return parameters;
      }

      public void setParameters(Parameters parameters) {
         this.parameters = parameters;
      }

      public Object getExtends() {
         return extendProfiles;
      }

      public void setExtends(Object extendProfiles) {
         this.extendProfiles = extendProfiles;
      }

      public Long getNumberLines() {
         return numberLines;
      }

      public void setNumberLines(Long numberLines) {
         this.numberLines = numberLines;
      }

      public Aliases getAliases() {
         return aliases;
      }

      public void setAliases(Aliases aliases) {
         this.aliases = aliases;
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this)
               .append("backendUris", backendUris)
               .append("connectTimeout", connectTimeout)
               .append("socketTimeout", socketTimeout)
               .append("authenticationToken", authenticationToken)
               .append("authenticationUser", authenticationUser)
               .append("authenticationPassword", authenticationPassword)
               .append("ansi", ansi)
               .append("parser", parser)
               .append("formatter", formatter)
               .append("progress", progress)
               .append("query", query)
               .append("notQuery", notQuery)
               .append("timezone", timezone)
               .append("backend", backend)
               .append("parameters", parameters)
               .append("extendProfiles", extendProfiles)
               .append("numberLines", numberLines)
               .append("aliases", aliases)
               .toString();
      }

   }

   public static class Formatter {
      private String name;
      private String template;

      public String getName() {
         return name;
      }

      public void setName(String name) {
         this.name = name;
      }

      public String getTemplate() {
         return template;
      }

      public void setTemplate(String template) {
         this.template = template;
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("name", name).append("template", template).toString();
      }
   }

}
