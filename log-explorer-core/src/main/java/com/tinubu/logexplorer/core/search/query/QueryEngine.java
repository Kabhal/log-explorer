/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

/**
 * Filters search results with supported queries from {@link QuerySpecification}.
 */
public class QueryEngine {

   /**
    * Filters search result with query specifications.
    *
    * @param searchResult search result to filter
    * @param querySpecification queries
    *
    * @return filtered search results.
    */
   public SearchResult filter(SearchResult searchResult, QuerySpecification querySpecification) {
      return searchResult.filterResults(attributes -> filterResults(attributes, querySpecification));
   }

   /**
    * Filters search results with specified {@code querySet}.
    *
    * @param results search results to filter
    * @param querySpecification query set to match
    *
    * @return {@code true} if log line matches the query
    */
   private boolean filterResults(Attributes results, QuerySpecification querySpecification) {
      for (SimpleQuery simpleQuery : querySpecification.querySet(SimpleQuery.class)) {
         if (!match(results, simpleQuery)) {
            return false;
         }
      }
      for (SimpleQuery notSimpleQuery : querySpecification.notQuerySet(SimpleQuery.class)) {
         if (match(results, notSimpleQuery)) {
            return false;
         }
      }
      for (UniformQuery uniformQuery : querySpecification.querySet(UniformQuery.class)) {
         if (!match(results, uniformQuery)) {
            return false;
         }
      }
      for (UniformQuery notUniformQuery : querySpecification.notQuerySet(UniformQuery.class)) {
         if (match(results, notUniformQuery)) {
            return false;
         }
      }
      return true;
   }

   /**
    * Matching algorithm for a given {@code query}.
    *
    * @param results attributes to match against query
    * @param query query to apply
    *
    * @return {@code true} if query matches the attributes
    */
   protected boolean match(Attributes results, Query query) {
      return query.match(results);
   }

}
