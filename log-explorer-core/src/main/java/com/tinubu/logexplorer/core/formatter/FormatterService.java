/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.formatter;

import java.time.ZoneId;

import com.tinubu.logexplorer.core.config.Configuration.Parameters;

/**
 * Extension point interface for formatter plugins.
 */
public interface FormatterService {

   /**
    * Uniquely identifies this formatter.
    *
    * @return formatter name
    */
   String name();

   /**
    * Builds a new formatter instance with the specified parameters.
    *
    * @param parameters plugin parameters
    * @param timezone general timezone for display
    * @param template optional template for formatter. It can be mandatory depending on plugin
    */
   Formatter instance(Parameters parameters, ZoneId timezone, String template);

}
