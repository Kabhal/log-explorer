/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.extension.parameter.completers;

import static java.util.Comparator.comparingInt;
import static java.util.function.BinaryOperator.maxBy;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Convenient class to cache chained completion candidates.
 * Cache is loaded, for the current partial command line, only if no cache entry matches a prefix of the
 * partial command line. The cached candidates are then manually filtered to match the provided partial
 * command line.
 * So the cache maximum size is the maximum number of unique partial command line prefixes to cache.
 *
 * @implNote If prefix cache key has length difference with partial command line greater than {@value
 *       MAX_CACHE_PREFIX_LENGTH_GAP}, the manual computation is manually inserted into the cache for
 *       optimization purpose.
 * @see EnforceMinimumInputSizeCompleter to enforce a minimum number of partial command line characters
 */
public class CacheCompleter implements Completer {

   private static final long DEFAULT_CACHE_MAXIMUM_SIZE = 10;
   /**
    * Cache is filled with manually computed candidates for optimization purpose when
    * key length gap is greater to this value.
    */
   private static final long MAX_CACHE_PREFIX_LENGTH_GAP = 5;

   private LoadingCache<String, List<String>> cache;
   /** Cache maximum number of items. */
   private long cacheMaximumSize;
   /** Cache duration (in seconds) after writes. */
   private long cacheDuration;

   public CacheCompleter(Completer chainedCompleter, long cacheMaximumSize, long cacheDuration) {
      this.cache = CacheBuilder
            .newBuilder()
            .maximumSize(cacheMaximumSize)
            .expireAfterWrite(cacheDuration, TimeUnit.SECONDS)
            .build(new CacheLoader<>() {
               @Override
               public List<String> load(String key) {
                  return chainedCompleter.apply(key);
               }
            });
      this.cacheMaximumSize = cacheMaximumSize;
      this.cacheDuration = cacheDuration;
   }

   public CacheCompleter(Completer chainedCompleter, long cacheDuration) {
      this(chainedCompleter, DEFAULT_CACHE_MAXIMUM_SIZE, cacheDuration);
   }

   @Override
   public List<String> apply(String partial) {
      String partialKey = partial == null ? "" : partial;

      Optional<String> prefixCacheKey = cache.asMap().keySet().stream()
            .filter(partialKey::startsWith)
            .reduce(maxBy(comparingInt(String::length)));

      return prefixCacheKey.map(prefixKey -> {
         List<String> candidates = getCache(prefixKey);
         List<String> partialMatchingCandidates =
               candidates.stream().filter(candidate -> candidate.startsWith(partialKey)).collect(toList());

         if (partialKey.length() - prefixKey.length() > MAX_CACHE_PREFIX_LENGTH_GAP) {
            cache.put(partialKey, partialMatchingCandidates);
         }

         return partialMatchingCandidates;
      }).orElseGet(() -> getCache(partialKey));
   }

   private List<String> getCache(String key) {
      try {
         return cache.get(key);
      } catch (ExecutionException e) {
         throw new IllegalStateException(e);
      }
   }
}
