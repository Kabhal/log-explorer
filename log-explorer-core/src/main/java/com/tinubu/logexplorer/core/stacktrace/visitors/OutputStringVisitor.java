/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace.visitors;

import static java.lang.System.lineSeparator;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.Validate.notNull;

import java.util.List;

import com.tinubu.logexplorer.core.stacktrace.StackTrace;
import com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType;
import com.tinubu.logexplorer.core.stacktrace.StackTraceException;
import com.tinubu.logexplorer.core.stacktrace.StackTraceExceptionVisitor;
import com.tinubu.logexplorer.core.stacktrace.TracedMethod;

/**
 * {@link StackTrace} visitor generating a formatted string.
 * The same visitor instance should be visited several times with each stack trace exception, before
 * retrieving resulting string.
 * Calls {@link OutputStringVisitor#toString()} to retrieved generated string once visitor has been applied.
 */
public class OutputStringVisitor implements StackTraceExceptionVisitor {

   protected final TracedMethodFilter tracedMethodFilter;

   protected StringBuilder output = new StringBuilder();

   public OutputStringVisitor() {
      this(new DefaultTracedMethodFilter());
   }

   public OutputStringVisitor(TracedMethodFilter tracedMethodFilter) {
      this.tracedMethodFilter = notNull(tracedMethodFilter, "tracedMethodFilter must not be null");
   }

   @Override
   public void visit(StackTraceException exception, ExceptionType exceptionType) {
      if (exceptionType.has(ExceptionType.CAUSE_EXCEPTION)) {
         outputCausedByException(exception);
      } else if (exceptionType.has(ExceptionType.WRAPPING_EXCEPTION)) {
         outputWrappedByException(exception);
      } else {
         outputRootException(exception);
      }
   }

   protected void outputRootException(StackTraceException exception) {
      output
            .append(filterExceptionType(exception, exception.type().name()))
            .append(": ")
            .append(filterExceptionMessage(exception, exception.message()))
            .append(lineSeparator());
      outputTracedMethods(exception, exception.tracedMethods());
   }

   protected void outputCausedByException(StackTraceException exception) {
      output
            .append("   ")
            .append(filterExceptionAssociation(exception, "Caused by"))
            .append(": ")
            .append(filterExceptionType(exception, exception.type().name()))
            .append(": ")
            .append(filterExceptionMessage(exception, exception.message()))
            .append(lineSeparator());
      outputTracedMethods(exception, exception.tracedMethods());
   }

   protected String filterExceptionAssociation(StackTraceException exception, String exceptionAssociation) {
      return exceptionAssociation;
   }

   protected void outputWrappedByException(StackTraceException exception) {
      output
            .append("   ")
            .append(filterExceptionAssociation(exception, "Wrapped by"))
            .append(": ")
            .append(filterExceptionType(exception, exception.type().name()))
            .append(": ")
            .append(filterExceptionMessage(exception, exception.message()))
            .append(lineSeparator());
      outputTracedMethods(exception, exception.tracedMethods());
   }

   protected String filterExceptionMessage(StackTraceException exception, String exceptionMessage) {
      return exceptionMessage;
   }

   protected String filterExceptionType(StackTraceException exception, String exceptionType) {
      if (exception.causedBy().isEmpty() && exception.wrappedBy().isPresent()) {
         return filterQualifiedExceptionType(exception, exceptionType, "[Root cause]");
      } else if (exception.wrappedBy().isEmpty() && exception.causedBy().isPresent()) {
         return filterQualifiedExceptionType(exception, exceptionType, "[Actual exception]");
      } else {
         return filterQualifiedExceptionType(exception, exceptionType, "");
      }
   }

   protected String filterQualifiedExceptionType(StackTraceException exception,
                                                 String exceptionType,
                                                 String qualifier) {
      return (isEmpty(qualifier) ? "" : (qualifier + " ")) + exceptionType;
   }

   protected void outputTracedMethods(StackTraceException exception, List<TracedMethod> tracedMethods) {
      tracedMethodFilter.resetCounter();
      tracedMethods
            .stream()
            .filter(tracedMethod -> filterTracedMethod(exception, tracedMethod))
            .forEach(tracedMethod -> outputTracedMethod(exception, tracedMethod));
   }

   protected void outputTracedMethod(StackTraceException exception, TracedMethod tracedMethod) {
      output.append("      ").append(filterTracedMethodPrefix(exception, tracedMethod, "at")).append(" ")
            .append(filterTracedMethodClassName(exception, tracedMethod, tracedMethod.className().name()))
            .append(".")
            .append(filterTracedMethodName(exception, tracedMethod, tracedMethod.methodName()))
            .append("(")
            .append(filterTracedMethodFileName(exception, tracedMethod, tracedMethod.fileName()));
      tracedMethod.line().ifPresent(line -> {
         output.append(":").append(filterTracedMethodLine(exception, tracedMethod, line));
      });
      output.append(")").append(lineSeparator());
   }

   protected String filterTracedMethodPrefix(StackTraceException exception,
                                             TracedMethod tracedMethod,
                                             String prefix) {
      return prefix;
   }

   protected String filterTracedMethodClassName(StackTraceException exception,
                                                TracedMethod tracedMethod,
                                                String className) {
      return className;
   }

   protected String filterTracedMethodName(StackTraceException exception,
                                           TracedMethod tracedMethod,
                                           String methodName) {
      return methodName;
   }

   protected String filterTracedMethodFileName(StackTraceException exception,
                                               TracedMethod tracedMethod,
                                               String fileName) {
      return fileName;
   }

   protected String filterTracedMethodLine(StackTraceException exception,
                                           TracedMethod tracedMethod,
                                           int methodLine) {
      return Integer.toString(methodLine);
   }

   protected boolean filterTracedMethod(StackTraceException exception, TracedMethod tracedMethod) {
      return tracedMethodFilter.filter(exception, tracedMethod);
   }

   public String toString() {
      return output.toString();
   }
}
