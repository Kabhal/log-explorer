/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.backend;

import java.util.function.BiConsumer;

import com.tinubu.logexplorer.core.extension.Extension;
import com.tinubu.logexplorer.core.search.QueryDateRange;
import com.tinubu.logexplorer.core.search.SearchResult;
import com.tinubu.logexplorer.core.search.query.QuerySpecification;

/**
 * Backend interface.
 */
public interface Backend extends Extension, AutoCloseable {

   /**
    * Searches backend.
    *
    * @param action callback to apply search results. Passed query set can be original or adapted as
    *       uniform querying is made later in callback
    * @param querySpecification query set
    * @param dateRange search date range
    * @param numberLines maximum number of the latest lines to return
    * @param followMode enable follow mode
    */
   void search(BiConsumer<SearchResult, QuerySpecification> action,
               QuerySpecification querySpecification,
               QueryDateRange dateRange,
               Long numberLines,
               boolean followMode);

   /**
    * Opportunity to close the backend before destruction.
    */
   void close();

}
