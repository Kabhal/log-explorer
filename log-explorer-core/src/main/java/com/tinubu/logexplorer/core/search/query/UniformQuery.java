/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.logexplorer.core.lang.TypeConverter.booleanValue;
import static com.tinubu.logexplorer.core.lang.TypeConverter.stringValue;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.GREATER_THAN;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.GREATER_THAN_OR_EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.LESS_THAN;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.LESS_THAN_OR_EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.MATCH;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.NOT_EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.NOT_MATCH;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.regex.Pattern;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.tinubu.commons.lang.mapper.EnumMapper;
import com.tinubu.logexplorer.core.lang.TypeConverter.RegexpFlag;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

// TODO field exists, not exists : UNARY, BINARY ? syntaxe ?
public class UniformQuery implements Query {

   /**
    * "Null" special operand value to match {@code null} attribute values.
    */
   public static final NullOperand NULL = new NullOperand();

   private static final Map<OperationReference, BiFunction<Object, Object, Boolean>> OPERATIONS =
         new HashMap<>() {{
            put(new OperationReference(EQUAL, String.class), UniformQuery::stringEqualAttribute);
            put(new OperationReference(NOT_EQUAL, String.class), UniformQuery::stringNotEqualAttribute);
            put(new OperationReference(EQUAL, Long.class), UniformQuery::longEqualAttribute);
            put(new OperationReference(NOT_EQUAL, Long.class), UniformQuery::longNotEqualAttribute);
            put(new OperationReference(EQUAL, Double.class), UniformQuery::doubleEqualAttribute);
            put(new OperationReference(NOT_EQUAL, Double.class), UniformQuery::doubleNotEqualAttribute);
            put(new OperationReference(EQUAL, Boolean.class), UniformQuery::booleanEqualAttribute);
            put(new OperationReference(NOT_EQUAL, Boolean.class), UniformQuery::booleanNotEqualAttribute);
            put(new OperationReference(MATCH, String.class), UniformQuery::stringMatchAttribute);
            put(new OperationReference(NOT_MATCH, String.class), UniformQuery::stringNotMatchAttribute);
            put(new OperationReference(EQUAL, Pattern.class), UniformQuery::patternEqualAttribute);
            put(new OperationReference(NOT_EQUAL, Pattern.class), UniformQuery::patternNotEqualAttribute);
            put(new OperationReference(MATCH, Pattern.class), UniformQuery::patternMatchAttribute);
            put(new OperationReference(NOT_MATCH, Pattern.class), UniformQuery::patternNotMatchAttribute);
            put(new OperationReference(EQUAL, NullOperand.class), UniformQuery::nullEqualAttribute);
            put(new OperationReference(NOT_EQUAL, NullOperand.class), UniformQuery::nullNotEqualAttribute);
            put(new OperationReference(LESS_THAN, Long.class), UniformQuery::longLessThanAttribute);
            put(new OperationReference(LESS_THAN_OR_EQUAL, Long.class),
                UniformQuery::longLessThanOrEqualAttribute);
            put(new OperationReference(GREATER_THAN, Long.class), UniformQuery::longGreaterThanAttribute);
            put(new OperationReference(GREATER_THAN_OR_EQUAL, Long.class),
                UniformQuery::longGreaterThanOrEqualAttribute);
            put(new OperationReference(LESS_THAN, Double.class), UniformQuery::doubleLessThanAttribute);
            put(new OperationReference(LESS_THAN_OR_EQUAL, Double.class),
                UniformQuery::doubleLessThanOrEqualAttribute);
            put(new OperationReference(GREATER_THAN, Double.class), UniformQuery::doubleGreaterThanAttribute);
            put(new OperationReference(GREATER_THAN_OR_EQUAL, Double.class),
                UniformQuery::doubleGreaterThanOrEqualAttribute);
         }};

   private final List<String> fieldPath;
   private final Operator operator;
   private final Object operand;
   private final Set<Option> options;

   private UniformQuery(List<String> fieldPath, Operator operator, Object operand, Set<Option> options) {
      this.fieldPath = notNull(fieldPath, "fieldPath");
      this.operator = notNull(operator, "operator");
      this.operand = notNull(operand, "operand");
      this.options = notNull(options, "options");

      operation(operator, operand);
   }

   public static UniformQuery of(List<String> fieldPath,
                                 Operator operator,
                                 Object operand,
                                 Set<Option> options) {
      return new UniformQuery(fieldPath, operator, operand, options);
   }

   public static UniformQuery fieldQuery(List<String> fieldPath,
                                         Operator operator,
                                         Object operand,
                                         Set<Option> options) {
      return new UniformQuery(notEmpty(fieldPath, "fieldPath"), operator, operand, options);
   }

   public static UniformQuery fieldQuery(List<String> fieldPath, Operator operator, Object operand) {
      return fieldQuery(fieldPath, operator, operand, emptySet());
   }

   public static UniformQuery anyFieldQuery(Operator operator, Object operand, Set<Option> options) {
      return new UniformQuery(emptyList(), operator, operand, options);
   }

   public static UniformQuery anyFieldQuery(Operator operator, Object operand) {
      return anyFieldQuery(operator, operand, emptySet());
   }

   @Override
   public String queryString() {
      StringBuilder query = new StringBuilder();

      if (fieldPath != null) {
         query.append('"').append(fieldPath.stream().collect(joining("."))).append('"');
      }
      query.append(operator.operatorString()).append(operandString());

      return query.toString();
   }

   public List<String> fieldPath() {
      return fieldPath;
   }

   public Operator operator() {
      return operator;
   }

   public Object operand() {
      return operand;
   }

   private String operandString() {
      if (operand instanceof String) {
         return '"' + operand.toString() + '"';
      } else if (operand instanceof Pattern) {
         return '/' + ((Pattern) operand).pattern() + '/' + RegexpFlag.flagString((Pattern) operand);
      } else {
         return operand.toString();
      }
   }

   public Set<Option> options() {
      return options;
   }

   public boolean hasOption(Option option) {
      return options.contains(notNull(option, "option"));
   }

   /**
    * @apiNote {@code null} attributes are only matched using {@link Operator#EQUAL}/{@link
    *       Operator#NOT_EQUAL} and {@code null} operand.
    * @implNote Attributes can have {@code null} values, so you can't use {@link Map#get(Object)} to
    *       retrieve values.
    */
   public boolean match(Attributes attributes) {
      return match(attributes, fieldPath);
   }

   private boolean match(Attributes attributes, List<String> fieldPath) {
      if (!fieldPath.isEmpty()) {
         if (fieldPath.size() > 1) {
            Object subAttributes = attributes.get(fieldPath.get(0));
            if (subAttributes instanceof Attributes) {
               return match((Attributes) subAttributes, fieldPath.stream().skip(1).collect(toList()));
            } else {
               return false;
            }
         } else {
            String rootField = fieldPath.get(0);
            if (attributes.containsKey(rootField)) {
               return matchAttribute(attributes.get(rootField));
            } else {
               return false;
            }
         }
      } else {
         return attributes.values().stream().anyMatch(this::matchAttribute);
      }
   }

   private boolean matchAttribute(Object attribute) {
      if (attribute instanceof Attributes) {
         return ((Attributes) attribute).values().stream().anyMatch(this::matchAttribute);
      } else {
         return operation(operator, operand).apply(attribute, operand);
      }
   }

   private static BiFunction<Object, Object, Boolean> operation(Operator operator, Object operand) {
      BiFunction<Object, Object, Boolean> operation =
            OPERATIONS.get(new OperationReference(operator, operand.getClass()));

      if (operation == null) {
         throw new IllegalStateException(String.format("Unsupported '%s' operator for '%s' operand type",
                                                       operator,
                                                       operand.getClass().getSimpleName()));
      }

      return operation;
   }

   private static boolean nullEqualAttribute(Object attribute, Object operand) {
      return attribute == null;
   }

   private static boolean nullNotEqualAttribute(Object attribute, Object operand) {
      return attribute != null;
   }

   private static boolean patternEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof String && ((Pattern) operand).matcher(stringValue(attribute)).matches();
   }

   private static boolean patternNotEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof String && !((Pattern) operand).matcher(stringValue(attribute)).matches();
   }

   private static boolean patternMatchAttribute(Object attribute, Object operand) {
      return attribute instanceof String && ((Pattern) operand).matcher(stringValue(attribute)).find();
   }

   private static boolean patternNotMatchAttribute(Object attribute, Object operand) {
      return attribute instanceof String && !((Pattern) operand).matcher(stringValue(attribute)).find();
   }

   private static boolean stringEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof String && stringValue(attribute).equals(operand);
   }

   private static boolean stringNotEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof String && !stringValue(attribute).equals(operand);
   }

   private static boolean booleanEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Boolean && booleanValue(attribute).equals(operand);
   }

   private static boolean booleanNotEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Boolean && !booleanValue(attribute).equals(operand);
   }

   private static boolean stringMatchAttribute(Object attribute, Object operand) {
      return attribute instanceof String && stringValue(attribute).contains((String) operand);
   }

   private static boolean stringNotMatchAttribute(Object attribute, Object operand) {
      return attribute instanceof String && !stringValue(attribute).contains((String) operand);
   }

   private static boolean longEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) operand).longValue() == ((Number) attribute).longValue();
   }

   private static boolean longNotEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) operand).longValue() != ((Number) attribute).longValue();
   }

   private static boolean doubleEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) operand).doubleValue() == ((Number) attribute).doubleValue();
   }

   private static boolean doubleNotEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) operand).doubleValue() != ((Number) attribute).doubleValue();
   }

   private static boolean longLessThanAttribute(Object attribute, Object operand) {
      return attribute instanceof Number && ((Number) attribute).longValue() < ((Number) operand).longValue();
   }

   private static boolean longLessThanOrEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).longValue() <= ((Number) operand).longValue();
   }

   private static boolean longGreaterThanAttribute(Object attribute, Object operand) {
      return attribute instanceof Number && ((Number) attribute).longValue() > ((Number) operand).longValue();
   }

   private static boolean longGreaterThanOrEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).longValue() >= ((Number) operand).longValue();
   }

   private static boolean doubleLessThanAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).doubleValue() < ((Number) operand).doubleValue();
   }

   private static boolean doubleLessThanOrEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).doubleValue() <= ((Number) operand).doubleValue();
   }

   private static boolean doubleGreaterThanAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).doubleValue() > ((Number) operand).doubleValue();
   }

   private static boolean doubleGreaterThanOrEqualAttribute(Object attribute, Object operand) {
      return attribute instanceof Number
             && ((Number) attribute).doubleValue() >= ((Number) operand).doubleValue();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      UniformQuery that = (UniformQuery) o;
      return Objects.equals(fieldPath, that.fieldPath)
             && operator == that.operator
             && operandEquals(that.operand);
   }

   public boolean operandEquals(Object otherOperand) {
      if (operand == otherOperand) return true;
      if (otherOperand == null || operand.getClass() != otherOperand.getClass()) return false;
      if (operand instanceof Pattern && otherOperand instanceof Pattern) {
         Pattern operandPattern = (Pattern) operand;
         Pattern otherOperandPattern = (Pattern) otherOperand;
         return operandPattern.pattern().equals(otherOperandPattern.pattern())
                && operandPattern.flags() == otherOperandPattern.flags();
      }
      return Objects.equals(operand, otherOperand);
   }

   @Override
   public int hashCode() {
      return Objects.hash(fieldPath, operator, operand);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("fieldPath", fieldPath)
            .append("operator", operator)
            .append("operand", operand)
            .toString();
   }

   public static class OperationReference {
      private final Operator operator;
      private final Class<?> operandClass;

      public OperationReference(Operator operator, Class<?> operandClass) {
         this.operator = operator;
         this.operandClass = operandClass;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         OperationReference that = (OperationReference) o;
         return operator == that.operator && Objects.equals(operandClass, that.operandClass);
      }

      @Override
      public int hashCode() {
         return Objects.hash(operator, operandClass);
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this)
               .append("operator", operator)
               .append("operandClass", operandClass)
               .toString();
      }
   }

   /**
    * Class holder to represent a concrete {@code null} operand value.
    */
   public static class NullOperand {
      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         return o != null && getClass() == o.getClass();
      }

      @Override
      public int hashCode() {
         return Objects.hash();
      }

   }

   public enum Operator {
      EQUAL("="),
      NOT_EQUAL("!="),
      MATCH("~"),
      NOT_MATCH("!~"),
      LESS_THAN("<"),
      LESS_THAN_OR_EQUAL("<="),
      GREATER_THAN(">"),
      GREATER_THAN_OR_EQUAL(">=");

      private static final EnumMapper<Operator, String> operatorStringMapper =
            new EnumMapper<>(Operator.class, Operator::operatorString);

      private final String operatorString;

      Operator(String operatorString) {
         this.operatorString = operatorString;
      }

      public static Operator fromOperatorString(String operatorString) {
         return nullable(operatorStringMapper.map(operatorString)).orElseThrow(() -> new IllegalArgumentException(
               String.format("Unknown operator '%s'", operatorString)));
      }

      public String operatorString() {
         return operatorString;
      }

   }

   public enum Option {
      CASE_INSENSITIVE
   }

}
