/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * {@link StackTrace} sub-model representing a Java type with an optional package part and a class name.
 */
public class Type {

   /**
    * Optional package name.
    */
   private final String packageName;

   /**
    * Class name.
    */
   private final String className;

   /**
    * Builds a type from their elements.
    *
    * @param packageName optional package name
    * @param className class name
    */
   public Type(String packageName, String className) {
      this.packageName = packageName != null ? notBlank(packageName, "packageName") : null;
      this.className = notNull(className, "className");
   }

   /**
    * Builds a type from their elements with no package.
    *
    * @param className class name
    */
   public Type(String className) {
      this(null, className);
   }

   /**
    * Builds a type from real class.
    *
    * @param clazz class
    * @param <T> class type
    */
   public <T> Type(Class<T> clazz) {
      this(clazz.getPackageName(), clazz.getSimpleName());
   }

   public String packageName() {
      return packageName;
   }

   public String className() {
      return className;
   }

   public String name() {
      return (packageName != null ? packageName + "." : "") + className;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Type type = (Type) o;
      return Objects.equals(packageName, type.packageName) && Objects.equals(className, type.className);
   }

   @Override
   public int hashCode() {
      return Objects.hash(packageName, className);
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("packageName", packageName)
            .append("className", className)
            .toString();
   }
}
