/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.formatter;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.extension.parameter.ParameterDescription;
import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

/**
 * Virtual {@link Formatter} which is a chain of responsibility of multiple formatters.
 * If any exception occurs in the previous formatter, the following formatter is tried.
 * Formatters should throw an exception if the format is not recognized, so multiple log sources can be
 * requested in one call and parsed by multiple formatters.
 * It's a good practice to always add a generic formatter (like {@code raw}) and {@code noop} formatter at the
 * end of the list.
 * Formatter rejections are logged at debug level.
 */
public class FormatterChain extends AbstractList<Formatter> implements Formatter {

   private static final Logger logger = LoggerFactory.getLogger(FormatterChain.class);

   private final List<Formatter> formatters;

   private FormatterChain(List<Formatter> formatters) {
      this.formatters = formatters;
   }

   public static FormatterChain empty() {
      return new FormatterChain(new ArrayList<>());
   }

   public FormatterChain withFormatter(Formatter formatter) {
      return new FormatterChain(Stream.concat(stream(), Stream.of(formatter)).collect(toList()));
   }

   @Override
   public void format(StringBuilder stringBuilder, Attributes attributes) {
      for (Formatter formatter : formatters) {
         try {
            StringBuilder formatterResult = new StringBuilder();
            formatter.format(formatterResult, attributes);
            stringBuilder.append(formatterResult);
            break;
         } catch (Exception e) {
            logger.debug("Formatter '{}' error", formatter, e);
         }
      }
   }

   @Override
   public List<ParameterDescription<?>> parameters() {
      List<ParameterDescription<?>> parameters = new ArrayList<>();
      for (Formatter formatter : formatters) {
         parameters.addAll(formatter.parameters());
      }
      return parameters;
   }

   @Override
   public Formatter get(int index) {
      return formatters.get(index);
   }

   @Override
   public int size() {
      return formatters.size();
   }

   @Override
   public String displayString() {
      return "Formatter chain : " + formatters.stream().map(Formatter::displayString).collect(joining(" > "));
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("formatters", formatters).toString();
   }
}
