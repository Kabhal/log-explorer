/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.export.fs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.fusesource.jansi.Ansi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.logexplorer.core.export.Exporter;
import com.tinubu.logexplorer.core.formatter.Formatter;
import com.tinubu.logexplorer.core.search.SearchResult;

/**
 * Exports results to an output file in specified append mode.
 */
public class FsExporter implements Exporter {
   private static final Logger logger = LoggerFactory.getLogger(FsExporter.class);

   private final File file;
   private final Formatter formatter;
   private final BufferedWriter outputFileWriter;

   /**
    * @implNote Using {@link File::getAbsoluteFile} to force resolution of relative path against {@code
    *       user.dir} property.
    */
   public FsExporter(File file, boolean append, Formatter formatter) {
      this.file = file.getAbsoluteFile();
      this.formatter = formatter;

      logger.debug("Export search results to file {} using {} formatter",
                   this.file,
                   this.formatter.displayString());

      try {
         outputFileWriter = new BufferedWriter(new FileWriter(this.file, append));
      } catch (IOException e) {
         throw new IllegalStateException(String.format("Can't open output file '%s'", this.file.getPath()),
                                         e);
      }
   }

   @Override
   public void export(SearchResult result) {
      StringBuilder resultBuffer = new StringBuilder();
      result.results().forEach(attrs -> {
         disableAnsi(() -> formatter.format(resultBuffer, attrs));
         resultBuffer.append(System.lineSeparator());
      });

      try {
         outputFileWriter.write(resultBuffer.toString());
         outputFileWriter.flush();
      } catch (IOException e) {
         throw new IllegalStateException(String.format("Can't write to file '%s'", file.getPath()), e);
      }
   }

   @Override
   public void close() {
      if (outputFileWriter != null) {
         try {
            outputFileWriter.close();
         } catch (IOException e) {
            logger.debug(e.getMessage(), e);
         }
      }
   }

   private void disableAnsi(Runnable runnable) {
      boolean saveAnsiEnabled = Ansi.isEnabled();
      Ansi.setEnabled(false);
      runnable.run();
      Ansi.setEnabled(saveAnsiEnabled);
   }
}
