/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.extension.parameter;

import java.util.Collections;
import java.util.List;

import com.tinubu.logexplorer.core.extension.parameter.completers.Completer;

public class ParameterDescription<T> {

   private final String name;
   private final Class<T> type;
   private final String description;
   private final T defaultValue;
   private Completer completer = __ -> Collections.emptyList();

   public ParameterDescription(String name, Class<T> type, T defaultValue, String description) {
      this.name = name;
      this.type = type;
      this.defaultValue = defaultValue;
      this.description = description;
   }

   public ParameterDescription(String name, Class<T> type) {
      this(name, type, null, null);
   }

   public ParameterDescription(String name, Class<T> type, T defaultValue) {
      this(name, type, defaultValue, null);
   }

   public ParameterDescription(String name, Class<T> type, String description) {
      this(name, type, null, description);
   }

   /**
    * Evaluates the completer function to return candidate list for current partial command line, never {@code
    * null}.
    *
    * @param partial partial command line, or {@code null}
    *
    * @return completion candidates
    */
   public List<String> completes(String partial) {
      return completer.apply(partial);
   }

   public ParameterDescription<T> completer(Completer completer) {
      this.completer = completer;
      return this;
   }

   /**
    * Parameter name.
    *
    * @return parameter name
    */
   public String name() {
      return name;
   }

   /**
    * Parameter type
    *
    * @return parameter type
    */
   public Class<T> type() {
      return type;
   }

   /**
    * Optional parameter default value
    *
    * @return parameter type
    */
   public T defaultValue() {
      return defaultValue;
   }

   /**
    * Optional parameter description
    *
    * @return parameter description or {@code null}
    */
   public String description() {
      return description;
   }

}
