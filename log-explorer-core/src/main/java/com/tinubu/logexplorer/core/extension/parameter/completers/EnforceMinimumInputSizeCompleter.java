/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.extension.parameter.completers;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Chainable completer to call a chained completer only if a minimum number of character is available for
 * the partial command line, otherwise, returns an empty list.
 */
public class EnforceMinimumInputSizeCompleter implements Completer {

   private final int minimumInputSize;
   private final Completer chainedCompleter;

   public EnforceMinimumInputSizeCompleter(Completer chainedCompleter, int minimumInputSize) {
      this.minimumInputSize = minimumInputSize;
      this.chainedCompleter = chainedCompleter;
   }

   @Override
   public List<String> apply(String partial) {
      if (minimumInputSize > 0 && (partial == null || partial.length() < minimumInputSize)) {
         return emptyList();
      } else {
         return chainedCompleter.apply(partial);
      }
   }
}