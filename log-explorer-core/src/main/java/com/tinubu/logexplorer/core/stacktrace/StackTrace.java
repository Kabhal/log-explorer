/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.stacktrace;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType.ACTUAL_EXCEPTION;
import static com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType.CAUSE_EXCEPTION;
import static com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType.ROOT_CAUSE_EXCEPTION;
import static com.tinubu.logexplorer.core.stacktrace.StackTrace.ExceptionType.WRAPPING_EXCEPTION;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Stack trace model representation.
 * <p>
 * You can go through the stack either by :
 * <ul>
 *    <li>starting from {@link #actualException()} and following {@link StackTraceException#causedBy()} association</li>
 *    <li>starting from {@link #rootCauseException()} and following {@link StackTraceException#wrappedBy()} association</li>
 * </ul>
 * <p>
 * You can also use the visitor pattern to walk through the stack either by :
 * <ul>
 *    <li>starting from actual exception : {@link #actualExceptionVisitor(StackTraceExceptionVisitor)}</li>
 *    <li>starting from root cause exception : {@link #rootCauseExceptionVisitor(StackTraceExceptionVisitor)}</li>
 * </ul>
 */
public class StackTrace {

   private final List<StackTraceException> exceptions = new ArrayList<>();

   /**
    * Returns a parsed stack trace from text content.
    *
    * @param stackTrace stack trace to parse
    *
    * @return parsed stack trace
    *
    * @throws InvalidStackTraceException if stack trace has an invalid format
    */
   public static StackTrace parse(String stackTrace) {
      return new StackTraceParser().parse(notNull(stackTrace, "stackTrace"));
   }

   public void pushRootException(StackTraceException exception) {
      this.exceptions.add(exception);
   }

   public void pushCauseException(StackTraceException exception) {
      this.exceptions.add(chainCauseException(exception));
   }

   public void pushWrappingException(StackTraceException exception) {
      this.exceptions.add(chainWrappingException(exception));
   }

   private StackTraceException chainWrappingException(StackTraceException exception) {
      return peekException()
            .map(previousException -> {
               previousException.wrappedBy(exception);
               exception.causedBy(previousException);
               return exception;
            })
            .orElseThrow(() -> new InvalidStackTraceException(String.format(
                  "Inconsistent exception chain for '%s' > No root exception",
                  exception)));
   }

   private StackTraceException chainCauseException(StackTraceException exception) {
      return peekException()
            .map(previousException -> {
               previousException.causedBy(exception);
               exception.wrappedBy(previousException);
               return exception;
            })
            .orElseThrow(() -> new InvalidStackTraceException(String.format(
                  "Inconsistent exception chain for '%s' > No root exception",
                  exception)));
   }

   /**
    * Returns internal exceptions for testing purpose.
    * Stack trace exception should be access through {@link #actualException()} or {@link
    * #rootCauseException()}.
    *
    * @return exceptions list
    */
   List<StackTraceException> exceptions() {
      return exceptions;
   }

   /**
    * Always return the root cause exception from the stack trace, whatever the stack trace is a wrapped by or
    * a caused by chain.
    *
    * @return the root cause exception of the stack trace
    */
   public StackTraceException rootCauseException() {
      return rootException()
            .map(this::lastCause).orElseThrow(() -> new IllegalStateException("Stack trace is empty"));
   }

   public ExceptionType exceptionType(StackTraceException exception) {
      ExceptionType exceptionType = new ExceptionType();
      if (exception.causedBy().isEmpty()) {
         exceptionType.withType(ROOT_CAUSE_EXCEPTION);
      } else {
         exceptionType.withType(WRAPPING_EXCEPTION);
      }
      if (exception.wrappedBy().isEmpty()) {
         exceptionType.withType(ACTUAL_EXCEPTION);
      } else {
         exceptionType.withType(CAUSE_EXCEPTION);
      }
      return exceptionType;
   }

   /**
    * Visits stacktrace exceptions from root cause exception to actual exception.
    * Exception type chain can be either :
    * <ul>
    * <li>exception chain : {@link ExceptionType#ROOT_CAUSE_EXCEPTION RC} -> {@link ExceptionType#WRAPPING_EXCEPTION W}... -> {@link ExceptionType#WRAPPING_EXCEPTION W} + {@link ExceptionType#ACTUAL_EXCEPTION A}</li>
    * <li>simple exception : {@link ExceptionType#ROOT_CAUSE_EXCEPTION RC} + {@link ExceptionType#ACTUAL_EXCEPTION A}</li>
    * </ul>
    *
    * @param visitor visitor to apply
    * @param <T> visitor type
    *
    * @return visitor
    */
   public <T extends StackTraceExceptionVisitor> T rootCauseExceptionVisitor(T visitor) {
      Optional<StackTraceException> currentException = Optional.of(rootCauseException());
      while (currentException.isPresent()) {
         currentException
               .get()
               .visit(visitor, exceptionType(currentException.get()).withoutType(CAUSE_EXCEPTION));
         currentException = currentException.get().wrappedBy();
      }
      return visitor;
   }

   /**
    * Visits stacktrace exceptions from actual exception to root cause exception.
    * Exception type chain can be either :
    * <ul>
    * <li>exception chain : {@link ExceptionType#ACTUAL_EXCEPTION A} -> {@link ExceptionType#CAUSE_EXCEPTION C}... -> {@link ExceptionType#CAUSE_EXCEPTION C} + {@link ExceptionType#ROOT_CAUSE_EXCEPTION RC}</li>
    * <li>simple exception : {@link ExceptionType#ACTUAL_EXCEPTION A} + {@link ExceptionType#ROOT_CAUSE_EXCEPTION RC}</li>
    * </ul>
    *
    * @param visitor visitor to apply
    * @param <T> visitor type
    *
    * @return visitor
    */
   public <T extends StackTraceExceptionVisitor> T actualExceptionVisitor(T visitor) {
      Optional<StackTraceException> currentException = Optional.of(actualException());
      while (currentException.isPresent()) {
         currentException
               .get()
               .visit(visitor, exceptionType(currentException.get()).withoutType(WRAPPING_EXCEPTION));
         currentException = currentException.get().causedBy();
      }
      return visitor;
   }

   private StackTraceException lastCause(StackTraceException exception) {
      return exception.causedBy().map(this::lastCause).orElse(exception);
   }

   /**
    * Always return the actual exception from the stack trace, whatever the stack trace is a wrapped by or a
    * caused by chain.
    *
    * @return the actual exception of the stack trace
    */
   public StackTraceException actualException() {
      return peekException()
            .map(this::lastWrapping).orElseThrow(() -> new IllegalStateException("Stack trace is empty"));
   }

   private StackTraceException lastWrapping(StackTraceException exception) {
      return exception.wrappedBy().map(this::lastWrapping).orElse(exception);
   }

   private Optional<StackTraceException> rootException() {
      return ofNullable(exceptions.get(0));
   }

   private Optional<StackTraceException> peekException() {
      return exceptions.isEmpty() ? empty() : of(exceptions.get(exceptions.size() - 1));
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this).append("exceptions", exceptions).toString();
   }

   public static class ExceptionType {
      /** Actual exception (outer-most exception). */
      public static final int ACTUAL_EXCEPTION = 0x01;
      /** Caused-by exception (inner exception). */
      public static final int CAUSE_EXCEPTION = 0x02;
      /** Wrapped-by exception (inner exception). */
      public static final int WRAPPING_EXCEPTION = 0x04;
      /** Root cause exception (inner-most exception). */
      public static final int ROOT_CAUSE_EXCEPTION = 0x08;

      private int value = 0x0;

      ExceptionType() {}

      ExceptionType(int exceptionType) {
         this.value = exceptionType;
      }

      ExceptionType(ExceptionType exceptionType) {
         this(exceptionType.value);
      }

      public ExceptionType withType(int exceptionType) {
         this.value |= exceptionType;
         return this;
      }

      public ExceptionType withoutType(int exceptionType) {
         this.value &= ~exceptionType;
         return this;
      }

      public boolean has(int exceptionType) {
         return (this.value & exceptionType) != 0;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         ExceptionType that = (ExceptionType) o;
         return value == that.value;
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }

      @Override
      public String toString() {
         return new ToStringBuilder(this).append("value", String.join(",", valueAsStrings())).toString();
      }

      private List<String> valueAsStrings() {
         List<String> stringValues = new ArrayList<>();
         if (has(ACTUAL_EXCEPTION)) {
            stringValues.add("ACTUAL_EXCEPTION");
         }
         if (has(CAUSE_EXCEPTION)) {
            stringValues.add("CAUSE_EXCEPTION");
         }
         if (has(ROOT_CAUSE_EXCEPTION)) {
            stringValues.add("ROOT_CAUSE_EXCEPTION");
         }
         if (has(WRAPPING_EXCEPTION)) {
            stringValues.add("WRAPPING_EXCEPTION");
         }
         return stringValues;
      }
   }
}
