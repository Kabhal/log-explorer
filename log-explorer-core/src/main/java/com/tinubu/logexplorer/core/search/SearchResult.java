/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.tinubu.commons.lang.util.Pair;

/**
 * Abstracts backend result.
 */
public class SearchResult implements Serializable {
   private final List<Attributes> results;
   private final long count;
   private final Long total;

   public SearchResult(List<Attributes> results, Long total) {
      this.results = notNull(results, "results");
      this.count = results.size();
      this.total = total;
   }

   public SearchResult(List<Attributes> results) {
      this(results, null);
   }

   /**
    * Copy constructor.
    */
   public SearchResult(SearchResult searchResult) {
      this(new ArrayList<>(searchResult.results), searchResult.total);
   }

   /**
    * Returns results.
    * If an attribute value is an attributes sub-node, it must be of type {@link Attributes}.
    */
   public List<Attributes> results() {
      return results;
   }

   public SearchResult mapResults(Function<Attributes, Attributes> resultMapper) {
      return new SearchResult(results.stream().map(resultMapper).collect(Collectors.toList()), total);
   }

   /**
    * @apiNote Arbitrarily filtering results will make {@link #total} inconsistent. We decide to keep
    *       the total unchanged because it's still a relevant value to represent the number of log entries to
    *       download before filtering.
    */
   public SearchResult filterResults(Predicate<Attributes> filter) {
      return new SearchResult(results.stream().filter(filter).collect(Collectors.toList()), total);
   }

   /** Returns current page results count. */
   public long count() {
      return count;
   }

   /**
    * Returns total results count or {@code null} if total values is unsupported by the backend.
    */
   public Long total() {
      return total;
   }

   /**
    * A map of log entry attributes.
    * Attribute values can be of any type, and even {@code null}.
    * Elements are kept ordered.
    */
   public static class Attributes extends LinkedHashMap<String, Object> {
      public Attributes() {}

      public Attributes(Map<String, Object> attributes) {
         super(attributes);
      }

      public Attributes withAttribute(String key, Object value) {
         put(key, value);
         return this;
      }

      public Attributes withAttributes(Attribute... attributes) {
         for (Attribute attribute : attributes) {
            put(attribute.getKey(), attribute.getValue());
         }
         return this;
      }
   }

   /**
    * Single log entry attribute.
    */
   public static class Attribute extends Pair<String, Object> {
      private Attribute(String key, Object value) {
         super(notBlank(key, "key"), value);
      }

      public static Attribute of(String key, Object value) {
         return new Attribute(key, value);
      }
   }
}
