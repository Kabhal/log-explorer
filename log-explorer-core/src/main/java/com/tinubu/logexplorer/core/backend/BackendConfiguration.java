/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.backend;

import java.net.URI;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class BackendConfiguration {

   /** Backend URIs. Support depends on backend type. */
   private List<URI> backendUris;
   /** Connection timeout in ms. */
   private int connectTimeout;
   /** Socket timeout in ms. */
   private int socketTimeout;
   /** Optional authentication token. */
   private String authenticationToken;
   /** Optional basic authentication user. */
   private String authenticationUser;
   /** Optional basic authentication password. */
   private String authenticationPassword;

   public List<URI> backendUris() {
      return backendUris;
   }

   public BackendConfiguration backendUris(List<URI> backendUris) {
      this.backendUris = backendUris;
      return this;
   }

   public int connectTimeout() {
      return connectTimeout;
   }

   public BackendConfiguration connectTimeout(int connectTimeout) {
      this.connectTimeout = connectTimeout;
      return this;
   }

   public int socketTimeout() {
      return socketTimeout;
   }

   public BackendConfiguration socketTimeout(int socketTimeout) {
      this.socketTimeout = socketTimeout;
      return this;
   }

   public String authenticationToken() {
      return authenticationToken;
   }

   public BackendConfiguration authenticationToken(String authenticationToken) {
      this.authenticationToken = authenticationToken;
      return this;
   }

   public String authenticationUser() { return authenticationUser; }

   public BackendConfiguration authenticationUser(String authenticationUser) {
      this.authenticationUser = authenticationUser;
      return this;
   }

   public String authenticationPassword() { return authenticationPassword; }

   public BackendConfiguration authenticationPassword(String authenticationPassword) {
      this.authenticationPassword = authenticationPassword;
      return this;
   }

   @Override
   public String toString() {
      return new ToStringBuilder(this)
            .append("backendUris", backendUris)
            .append("connectTimeout", connectTimeout)
            .append("socketTimeout", socketTimeout)
            .append("authenticationToken", authenticationToken)
            .append("authenticationUser", authenticationUser)
            .append("authenticationPassword", authenticationPassword)
            .toString();
   }

}
