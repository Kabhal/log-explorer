/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.extension.parameter.completers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class EnforceMinimumInputSizeCompleterTest {
   @Test
   public void testWhenBlank() {
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 3).apply(null)).isEmpty();
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 3).apply("")).isEmpty();
   }

   @Test
   public void testWhenZeroLength() {
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 0).apply(null)).containsOnly(
            "true",
            "false");
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 0).apply("")).containsOnly(
            "true",
            "false");
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 0).apply("a")).containsOnly(
            "true",
            "false");
   }

   @Test
   public void testWhenMinimumNotReached() {
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 3).apply("ab")).isEmpty();
   }

   @Test
   public void testWhenMinimumReached() {
      assertThat(new EnforceMinimumInputSizeCompleter(new BooleanCompleter(), 3).apply("abc")).containsOnly(
            "true",
            "false");
   }

}