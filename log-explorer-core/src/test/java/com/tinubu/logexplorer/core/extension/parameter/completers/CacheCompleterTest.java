/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.extension.parameter.completers;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CacheCompleterTest {

   public static final List<String> SOURCE_CANDIDATES =
         Arrays.asList("world", "world-war", "xanax", "xray", "gammaray");

   @Mock(lenient = true)
   FixedCompleter sourceCompleter;

   @BeforeEach
   public void mockCompleter() {
      doReturn(SOURCE_CANDIDATES).when(sourceCompleter).apply(eq(null));
      doReturn(SOURCE_CANDIDATES).when(sourceCompleter).apply(eq(""));
      doAnswer(invocation -> {
         String partial = invocation.getArgument(0);
         return SOURCE_CANDIDATES
               .stream()
               .filter(candidate -> candidate.startsWith(partial))
               .collect(toList());
      }).when(sourceCompleter).apply(anyString());
   }

   @Test
   public void testWhenBlank() {
      CacheCompleter cacheCompleter = new CacheCompleter(sourceCompleter, 600);

      assertThat(cacheCompleter.apply(null)).containsOnlyElementsOf(SOURCE_CANDIDATES);
      assertThat(cacheCompleter.apply("")).containsOnlyElementsOf(SOURCE_CANDIDATES);

      verify(sourceCompleter, times(1)).apply("");
      verifyNoMoreInteractions(sourceCompleter);
   }

   @Test
   public void testWhenNominal() {
      CacheCompleter cacheCompleter = new CacheCompleter(sourceCompleter, 600);

      assertThat(cacheCompleter.apply("wor")).containsOnly("world", "world-war");
      assertThat(cacheCompleter.apply("world-w")).containsOnly("world-war");
      assertThat(cacheCompleter.apply("xa")).containsOnly("xanax");
      assertThat(cacheCompleter.apply("notexist")).isEmpty();

      verify(sourceCompleter, times(1)).apply("wor");
      verify(sourceCompleter, times(1)).apply("xa");
      verify(sourceCompleter, times(1)).apply("notexist");
      verifyNoMoreInteractions(sourceCompleter);
   }

   @Test
   public void testWhenSuffixMatchInsteadOfPrefix() {
      CacheCompleter cacheCompleter = new CacheCompleter(sourceCompleter, 600);

      assertThat(cacheCompleter.apply("ray")).isEmpty();

      verify(sourceCompleter, times(1)).apply("ray");
      verifyNoMoreInteractions(sourceCompleter);
   }

   @Test
   public void testWhenReusePrefix() {
      CacheCompleter cacheCompleter = new CacheCompleter(sourceCompleter, 600);

      assertThat(cacheCompleter.apply("wor")).containsOnly("world", "world-war");
      assertThat(cacheCompleter.apply("world")).containsOnly("world", "world-war");
      assertThat(cacheCompleter.apply("xana")).containsOnly("xanax");

      verify(sourceCompleter, times(1)).apply("wor");
      verify(sourceCompleter, times(1)).apply("xana");
      verifyNoMoreInteractions(sourceCompleter);
   }

   @Test
   public void testWhenReusePrefixWithLengthGapReached() {
      CacheCompleter cacheCompleter = new CacheCompleter(sourceCompleter, 600);

      assertThat(cacheCompleter.apply("w")).containsOnly("world", "world-war");
      assertThat(cacheCompleter.apply("world-war")).containsOnly("world-war");

      verify(sourceCompleter, times(1)).apply("w");
      verifyNoMoreInteractions(sourceCompleter);
   }

}