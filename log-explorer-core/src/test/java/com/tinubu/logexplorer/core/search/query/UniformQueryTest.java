/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.logexplorer.core.search.query;

import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.MATCH;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.NOT_EQUAL;
import static com.tinubu.logexplorer.core.search.query.UniformQuery.Operator.NOT_MATCH;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.logexplorer.core.search.SearchResult.Attributes;

class UniformQueryTest {

   @Test
   public void testBuildWhenNominal() {
      UniformQuery.of(singletonList("field"), EQUAL, "a string", emptySet());
      UniformQuery.of(emptyList(), EQUAL, "a string", emptySet());
      UniformQuery.fieldQuery(singletonList("field"), EQUAL, "a string");
      UniformQuery.anyFieldQuery(EQUAL, "a string");
   }

   @Test
   public void testBuildWhenBadParameter() {
      assertThatThrownBy(() -> UniformQuery.of(null, EQUAL, "a string", emptySet())).isInstanceOf(
            NullPointerException.class);
      assertThatThrownBy(() -> UniformQuery.of(singletonList("field"),
                                               null,
                                               "a string",
                                               emptySet())).isInstanceOf(NullPointerException.class);
      assertThatThrownBy(() -> UniformQuery.of(singletonList("field"), EQUAL, null, emptySet())).isInstanceOf(
            NullPointerException.class);
      assertThatThrownBy(() -> UniformQuery.fieldQuery(null, EQUAL, "a string")).isInstanceOf(
            NullPointerException.class);
      assertThatThrownBy(() -> UniformQuery.fieldQuery(emptyList(), EQUAL, "a string")).isInstanceOf(
            IllegalArgumentException.class);
   }

   @Test
   public void testFilterWhenFieldQuery() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("otherfield", "a string"))).isFalse();
   }

   @Test
   public void testFilterWhenSubFieldQuery() {
      UniformQuery query = UniformQuery.fieldQuery(Arrays.asList("field", "subfield"), EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field.subfield", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field",
                                                            new Attributes().withAttribute("subfield",
                                                                                           "a string")))).isTrue();
   }

   @Test
   public void testFilterWhenSubFieldQueryWithUnknownRoot() {
      UniformQuery query = UniformQuery.fieldQuery(Arrays.asList("unknown", "subfield"), EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field.subfield", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field",
                                                            new Attributes().withAttribute("subfield",
                                                                                           "a string")))).isFalse();
   }

   @Test
   public void testFilterWhenMultipleSubFieldQuery() {

      UniformQuery query =
            UniformQuery.fieldQuery(Arrays.asList("field", "subfield", "otherfield"), EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field.subfield.otherfield",
                                                            "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field",
                                                            new Attributes().withAttribute("subfield",
                                                                                           new Attributes().withAttribute(
                                                                                                 "otherfield",
                                                                                                 "a string"))))).isTrue();
   }

   @Test
   public void testFilterWhenAnyFieldQuery() {
      UniformQuery query = UniformQuery.anyFieldQuery(EQUAL, "a string");

      assertThat(query.match(new Attributes()
                                   .withAttribute("field1", "a string")
                                   .withAttribute("field2", "other string"))).isTrue();
      assertThat(query.match(new Attributes()
                                   .withAttribute("field1", "first string")
                                   .withAttribute("field2", "other string"))).isFalse();

      query = UniformQuery.anyFieldQuery(NOT_EQUAL, "a string");

      assertThat(query.match(new Attributes()
                                   .withAttribute("field1", "a string")
                                   .withAttribute("field2", "a string"))).isFalse();
      assertThat(query.match(new Attributes()
                                   .withAttribute("field1", "a string")
                                   .withAttribute("field2", "other string"))).isTrue();
      assertThat(query.match(new Attributes()
                                   .withAttribute("field1", "first string")
                                   .withAttribute("field2", "other string"))).isTrue();
   }

   @Test
   public void testFilterWhenAnyFieldQueryWithRecursiveAttributes() {
      UniformQuery query = UniformQuery.anyFieldQuery(EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field",
                                                            new Attributes().withAttribute("subfield",
                                                                                           new Attributes().withAttribute(
                                                                                                 "otherfield",
                                                                                                 "a string"))))).isTrue();

      query = UniformQuery.anyFieldQuery(NOT_EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field",
                                                            new Attributes().withAttribute("subfield",
                                                                                           new Attributes().withAttribute(
                                                                                                 "otherfield",
                                                                                                 "a string"))))).isFalse();
   }

   @Test
   public void testFilterWhenBlankStringOperand() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), MATCH, " ");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "astring"))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_MATCH, " ");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "astring"))).isTrue();
   }

   @Test
   public void testFilterWhenStringOperand() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "string a"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_EQUAL, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "string a"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), MATCH, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "string a"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_MATCH, "a string");

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "string a"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();
   }

   @Test
   public void testFilterWhenStringOperandWithStringConversion() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, "32");

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "32"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", 33))).isFalse();
   }

   @Test
   public void testFilterWhenPatternOperand() {
      UniformQuery query =
            UniformQuery.fieldQuery(singletonList("field"), EQUAL, Pattern.compile("a.*string"));

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "another string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "does not match"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_EQUAL, Pattern.compile("a.*string"));

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "another string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "does not match"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), MATCH, Pattern.compile("a.*string"));

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "another string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "does not match"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_MATCH, Pattern.compile("a.*string"));

      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "another string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "does not match"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "< a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a string >"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();
   }

   @Test
   public void testFilterWhenPatternOperandWithStringConversion() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, Pattern.compile("[0-9]+"));

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", 32.0))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "32"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "32.0"))).isFalse();
   }

   @Test
   public void testFilterWhenAnyOperand() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, 32L);

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "32"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_EQUAL, 32L);

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", 33))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "32"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      assertThatThrownBy(() -> UniformQuery.fieldQuery(singletonList("field"), MATCH, 32))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Unsupported 'MATCH' operator for 'Integer' operand type");
      assertThatThrownBy(() -> UniformQuery.fieldQuery(singletonList("field"), NOT_MATCH, 32))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Unsupported 'NOT_MATCH' operator for 'Integer' operand type");
   }

   @Test
   public void testFilterWhenNullOperand() {
      UniformQuery query = UniformQuery.fieldQuery(singletonList("field"), EQUAL, UniformQuery.NULL);

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isFalse();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isTrue();

      query = UniformQuery.fieldQuery(singletonList("field"), NOT_EQUAL, UniformQuery.NULL);

      assertThat(query.match(new Attributes().withAttribute("field", 32))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", "a string"))).isTrue();
      assertThat(query.match(new Attributes().withAttribute("field", null))).isFalse();

      assertThatThrownBy(() -> UniformQuery.fieldQuery(singletonList("field"), MATCH, UniformQuery.NULL))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Unsupported 'MATCH' operator for 'NullOperand' operand type");
      assertThatThrownBy(() -> UniformQuery.fieldQuery(singletonList("field"), NOT_MATCH, UniformQuery.NULL))
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("Unsupported 'NOT_MATCH' operator for 'NullOperand' operand type");
   }

}